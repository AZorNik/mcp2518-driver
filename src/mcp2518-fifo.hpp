#pragma once

#include <array>
#include "mcp2518-registers.hpp"

namespace azn::mcp2518 {

	namespace Fifo {
		using Number = uint8_t;
		inline constexpr uint8_t MinFifoNumber = 1;
		inline constexpr uint8_t MaxFifoNumber = 31;

		static constexpr bool isNumberOk(Fifo::Number num) { return (num >= MinFifoNumber) && (num <= MaxFifoNumber); }

		struct FifoRegisters_t {
			RegAddress con;
			RegAddress sta;
			RegAddress ua;
		};

		inline constexpr uint8_t FifosQty = 31;

		inline constexpr std::array<FifoRegisters_t, FifosQty> Registers {[]{
			std::array<FifoRegisters_t, FifosQty>fifos{};
			constexpr uint16_t BaseAddr = 0x5c;
			for (RegAddress i = 0; i < FifosQty; i++) {
				fifos[i] = {static_cast<RegAddress>(BaseAddr + i * 12)
						, static_cast<RegAddress>(BaseAddr + 4 + i * 12)
						, static_cast<RegAddress>(BaseAddr + 8 + i * 12)};
			}

			return fifos;
		}()};

		enum class Direction : uint8_t {
			Receive = 0,
			Transmit = 1,
		};

		enum class PayloadSize : uint8_t {
			Data8Bytes = 0,
			Data12Bytes = 1,
			Data16Bytes = 2,
			Data20Bytes = 3,
			Data24Bytes = 4,
			Data32Bytes = 5,
			Data48Bytes = 6,
			Data64Bytes = 7,
		};

		constexpr uint8_t PlSize2Bytes(PayloadSize plSizeCode) {
			constexpr uint8_t sizeInBytes[8] {8, 12, 16, 20, 24, 32, 48, 64};
			return sizeInBytes[static_cast<uint8_t>(plSizeCode)];
		}

		enum class Retransmission : uint8_t {
			NoRetransmission = 0,
			ThreeRetransmission = 1,
			UnlimitedRetransmission = 2,
			//UnlimitedRetransmission = 3,
		};

		struct Config {
			PayloadSize plSize {PayloadSize::Data8Bytes};
			uint8_t fifoSize {1};
			Retransmission retransmission {Retransmission::NoRetransmission};
			uint8_t priority {0};

			Direction direction {Direction::Transmit};
			bool autoRtrEnable {false};
			bool receiveMsgTimeStampEnable {false};
			bool transmitExhaustedInterruptEnable {false};
			bool overflowInterruptEnable {false};
			bool emptyFullInterruptEnable {false};
			bool halfEmptyFullInterruptEnable {false};
			bool notEmptyFullInterruptEnable {false};

			static constexpr auto PlSize = BitField<uint32_t, PayloadSize>(3, 29);
			static constexpr auto FifoSize = BitField<uint32_t>(5, 24);
			static constexpr auto Txat = BitField<uint32_t, Retransmission>(2, 21);
			static constexpr auto TxPriority = BitField<uint32_t>(5, 16);
			static constexpr auto Reset = BitField<uint32_t>(1, 10);
			static constexpr auto TxReq = BitField<uint32_t>(1, 9);
			static constexpr auto Uinc = BitField<uint32_t>(1, 8);
			static constexpr auto TxEn = BitField<uint32_t, Direction>(1, 7);
			static constexpr auto RtrEn = BitField<uint32_t, bool>(1, 6);
			static constexpr auto RxtsEn = BitField<uint32_t, bool>(1, 5);
			static constexpr auto TxatiEn = BitField<uint32_t, bool>(1, 4);
			static constexpr auto RxoviEn = BitField<uint32_t, bool>(1, 3);
			static constexpr auto TferffiEn = BitField<uint32_t, bool>(1, 2);
			static constexpr auto TfhrfhiEn = BitField<uint32_t, bool>(1, 1);
			static constexpr auto TfnrfniEn = BitField<uint32_t, bool>(1, 0);
		};

		struct Status {
			uint8_t meassageIndex;
			bool aboreted;
			bool lostArbitreation;
			bool transmitionError;
			bool overflow;
			bool emptyFull;
			bool halfEmptyFull;
			bool notEmptyFull;

			static constexpr auto FifoCi = BitField<uint32_t, uint8_t>(5, 8);
			static constexpr auto TxAbt = BitField<uint32_t, bool>(1, 7);
			static constexpr auto TxLarb = BitField<uint32_t, bool>(1, 6);
			static constexpr auto TxErr = BitField<uint32_t, bool>(1, 5);
			static constexpr auto TxAtIf = BitField<uint32_t, bool>(1, 4);
			static constexpr auto RxOvIf = BitField<uint32_t, bool>(1, 3);
			static constexpr auto TfeRffIf = BitField<uint32_t, bool>(1, 2);
			static constexpr auto TfhRfhIf = BitField<uint32_t, bool>(1, 1);
			static constexpr auto TfnRfnIf = BitField<uint32_t, bool>(1, 0);
		};
	} // namespace Fifo
} // namespace azn::Mcp2518
