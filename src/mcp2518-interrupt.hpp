#pragma once
#include <cstdint>
#include "bitField.hpp"


namespace azn::mcp2518{

	namespace Interrupt {
		enum class InterruptCode : uint8_t {
			TXQ = 0,
			Fifo1 = 1,
			// TODO add Fifo2 ... Fifo30
			Fifo31 = 31,
			NoInterrupt = 0x40,
			Error = 0x41,
			WakeUp = 0x42,
			FifoOverflow = 0x43,
			AddressError = 0x44,
			MabOverflow = 0x45,
			TbcOverflow = 0x46,
			OpModeChanged = 0x47,
			InvalidMessage = 0x48,
			TransmitEventFifo = 0x49,
			TransmitAttempt = 0x4a
		};

		struct Codes {
			uint8_t Rx;
			uint8_t Tx;
			uint8_t Filter;
			InterruptCode ICode;

			inline static constexpr auto RxMask = BitField<uint32_t>(7, 24);
			inline static constexpr auto TxMask = BitField<uint32_t>(7, 16);
			inline static constexpr auto FilterMask = BitField<uint32_t>(5, 8);
			inline static constexpr auto ICodeMask = BitField<uint32_t, InterruptCode>(7, 0);
		};

		enum class Main : uint16_t {
			TransmitFifo = 1,
			ReceiveFifo = 1 << 1,
			TbcOverflow = 1 << 2,
			OpModeChanged = 1 << 3,
			TransmitEventFifo = 1 << 4,
			EccError = 1 << 8,
			SpiCrcError = 1 << 9,
			TransmitAttempt = 1 << 10,
			ReceiveObjectOverflow = 1 << 11,
			SystemError = 1 << 12,
			CanBusError = 1 << 13,
			BusWakeUp = 1 << 14,
			InvalidMessage = 1 << 15,
		};

		struct Group {
			uint16_t flags {0};
			constexpr Group& set(Main interrupt) noexcept {
				flags |= static_cast<uint16_t>(interrupt);
				return *this;
			}
			constexpr Group& reset(Main interrupt) noexcept {
				flags &= ~(static_cast<uint16_t>(interrupt));
				return *this;
			}
			constexpr bool get(Main interrupt) noexcept {
				return flags & static_cast<uint16_t>(interrupt);
			}
		};


		namespace Flags {
			inline constexpr auto IvmIE = BitField<uint32_t, bool>(1, 31);
			inline constexpr auto ValIE = BitField<uint32_t, bool>(1, 30);
			inline constexpr auto CerrIE = BitField<uint32_t, bool>(1, 29);
			inline constexpr auto SerrIE = BitField<uint32_t, bool>(1, 28);
			inline constexpr auto RxovIE = BitField<uint32_t, bool>(1, 27);
			inline constexpr auto TxatIE = BitField<uint32_t, bool>(1, 26);
			inline constexpr auto SpicrcIE = BitField<uint32_t, bool>(1, 25);
			inline constexpr auto EccIE = BitField<uint32_t, bool>(1, 24);
			inline constexpr auto TefIE = BitField<uint32_t, bool>(1, 20);
			inline constexpr auto ModIE = BitField<uint32_t, bool>(1, 19);
			inline constexpr auto TbcIE = BitField<uint32_t, bool>(1, 18);
			inline constexpr auto RxIE = BitField<uint32_t, bool>(1, 17);
			inline constexpr auto TxIE = BitField<uint32_t, bool>(1, 16);
			inline constexpr auto IvmIF = BitField<uint32_t, bool>(1, 15);
			inline constexpr auto ValIF = BitField<uint32_t, bool>(1, 14);
			inline constexpr auto CerrIF = BitField<uint32_t, bool>(1, 13);
			inline constexpr auto SerrIF = BitField<uint32_t, bool>(1, 12);
			inline constexpr auto RxovIF = BitField<uint32_t, bool>(1, 11);
			inline constexpr auto TxatIF = BitField<uint32_t, bool>(1, 10);
			inline constexpr auto SpicrcIF = BitField<uint32_t, bool>(1, 9);
			inline constexpr auto EccIF = BitField<uint32_t, bool>(1, 8);
			inline constexpr auto TefIF = BitField<uint32_t, bool>(1, 4);
			inline constexpr auto ModIF = BitField<uint32_t, bool>(1, 3);
			inline constexpr auto TbcIF = BitField<uint32_t, bool>(1, 2);
			inline constexpr auto RxIF = BitField<uint32_t, bool>(1, 1);
			inline constexpr auto TxIF = BitField<uint32_t, bool>(1, 0);
		}
	} // namespace Interrupt

	constexpr Interrupt::Group operator|(Interrupt::Main lhs, Interrupt::Main rhs) noexcept {
		return Interrupt::Group().set(lhs).set(rhs);
	}

	constexpr Interrupt::Group operator|(Interrupt::Group g, Interrupt::Main interrupt) noexcept {
		return g.set(interrupt);
	}

} // namespace azn::Mcp2518
