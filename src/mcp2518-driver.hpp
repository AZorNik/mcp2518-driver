#pragma once
#include "mcp2518-fifo.hpp"
#include "mcp2518-filter.hpp"
#include "mcp2518-bitTime.hpp"
#include "mcp2518-interrupt.hpp"

namespace azn::mcp2518 {

class Driver {

public:
	using SpiRead = bool (*)(uint8_t* buffer, uint8_t qtyRead);
	using SpiWrite = bool (*)(const uint8_t* buffer, uint8_t qtyWrite);
	using SpiSelect = bool (*)(bool select);

	Driver(SpiRead read, SpiWrite write, SpiSelect select) noexcept;


	[[nodiscard]] bool reset(void) noexcept;
	[[nodiscard]] bool readReg(RegAddress addr, uint8_t* buffer, uint32_t bytesQty) noexcept;
	[[nodiscard]] bool writeReg(RegAddress addr, const uint8_t* buffer, uint32_t bytesQty) noexcept;
	[[nodiscard]] bool readReg(Sfr reg, uint32_t& buffer) noexcept;
	[[nodiscard]] bool writeReg(Sfr reg, const uint32_t& buffer) noexcept;

	[[nodiscard]] bool readMsgMem(RegAddress addr, uint32_t* buffer, uint32_t bytesQty) noexcept;
	[[nodiscard]] bool writeMsgMem(RegAddress addr, const uint32_t* buffer, uint32_t bytesQty) noexcept;


	[[nodiscard]] bool requestOpMod(Control::OperationMode mode) noexcept;
	[[nodiscard]] bool getOpMod(Control::OperationMode& mode) noexcept;

	[[nodiscard]] bool setOscConfig(const Osc::Config& config) noexcept;
	[[nodiscard]] bool getOscConfig(Osc::Config& config) noexcept;
	[[nodiscard]] bool getOscFlags(Osc::Flags& flags) noexcept;

	[[nodiscard]] bool setNominalBitTime(const BitTime& bitTime) noexcept;
	[[nodiscard]] bool setDataBitTime(const BitTime& bitTime) noexcept;
	[[nodiscard]] bool getNominalBitTime(BitTime& bitTime) noexcept;
	[[nodiscard]] bool getDataBitTime(BitTime& bitTime) noexcept;

	[[nodiscard]] bool loadFrame(Fifo::Number fifoNumber, const can::Can2Frame& frame) noexcept;
	[[nodiscard]] bool sendFrame(Fifo::Number fifoNumber, const can::Can2Frame& frame) noexcept;
	[[nodiscard]] bool receiveFrame(Fifo::Number fifoNumber, can::Can2Frame& frame) noexcept;

	[[nodiscard]] bool loadFrame(Fifo::Number fifoNumber, const can::FdFrame& frame) noexcept;
	[[nodiscard]] bool sendFrame(Fifo::Number fifoNumber, const can::FdFrame& frame) noexcept;
	[[nodiscard]] bool receiveFrame(Fifo::Number fifoNumber, can::FdFrame& frame) noexcept;

	[[nodiscard]] bool loadFrame(Fifo::Number fifoNumber, const can::Frame& frame) noexcept;
	[[nodiscard]] bool sendFrame(Fifo::Number fifoNumber, const can::Frame& frame) noexcept;
	[[nodiscard]] bool receiveFrame(Fifo::Number fifoNumber, can::Frame& frame) noexcept;

	[[nodiscard]] bool setFifoConfig(Fifo::Number fifoNumber, const Fifo::Config& config) noexcept;
	[[nodiscard]] bool getFifoConfig(Fifo::Number fifoNumber, Fifo::Config& config) noexcept;

	[[nodiscard]] bool writeMsgObj(Fifo::Number fifoNumber, const TransmitMsgObj& obj) noexcept;
	[[nodiscard]] bool readMsgObj(Fifo::Number fifoNumber, ReceiveMsgObj& obj) noexcept;
	[[nodiscard]] bool msgSendRequest(Fifo::Number fifoNumber) noexcept;

	[[nodiscard]] bool filterSetEnable(uint8_t filterNumber, bool enable) noexcept;
	[[nodiscard]] bool filterSetConfig(uint8_t filterNumber, const Filter::Config& config) noexcept;
	[[nodiscard]] bool filterGetConfig(uint8_t filterNumber, Filter::Config& config) noexcept;

	[[nodiscard]] bool ioSetConfig(const Io::Config& config) noexcept;
	[[nodiscard]] bool ioGetConfig(Io::Config& config) noexcept;
	[[nodiscard]] bool ioSetPinState(Io::PinId pin, Io::PinState state) noexcept;
	[[nodiscard]] bool ioGetPinState(Io::PinId pin, Io::PinState& state) noexcept;


	[[nodiscard]] bool interruptGetCodes(Interrupt::Codes& codes) noexcept;
	[[nodiscard]] bool interruptEnable(Interrupt::Main interrupt) noexcept;
	[[nodiscard]] bool interruptDisable(Interrupt::Main interrupt) noexcept;
	[[nodiscard]] bool interruptEnable(Interrupt::Group interrupts) noexcept;
	[[nodiscard]] bool interruptDisable(Interrupt::Group interrupts) noexcept;
	[[nodiscard]] bool interruptEnableAll(void) noexcept;
	[[nodiscard]] bool interruptDisableAll(void) noexcept;
	[[nodiscard]] bool interruptGetFlags(Interrupt::Group& interrupts) noexcept;
	[[nodiscard]] bool interruptGetRxStatus(uint32_t& status) noexcept;
	[[nodiscard]] bool interruptGetOverflowStatus(uint32_t& status) noexcept;
	[[nodiscard]] bool interruptGetTxStatus(uint32_t& status) noexcept;
	[[nodiscard]] bool interruptGetTxAttemptStatus(uint32_t& status) noexcept;

	[[nodiscard]] bool errorsGetCounters(Errors::Trec& status) noexcept;

private:

	static constexpr uint16_t byteswap(uint16_t val) noexcept {
		return ((val & 0xff) << 8) | ((val & 0xff00) >> 8);
	}

	SpiRead read;
	SpiWrite write;
	SpiSelect select;

	enum SpiCommand {
		Reset = 0b0,
		Read = 0b11,
		Write = 0b10,
		ReadCrc = 0b1011,
		WriteCrc = 0b1010,
		WriteSafe = 0b1100,
	};

	static constexpr uint8_t AddressOffset = 4;
	static constexpr uint8_t MsgMemWordSize = 4;
	static constexpr uint16_t MsgMemStart = 0x400;

	static uint16_t bytesQty2WordsQty(uint16_t bytesQty) noexcept;
	bool getFifoUserAddress(Fifo::Number fifoNumber, RegAddress& ua) noexcept;
	bool incFifoUserAddress(Fifo::Number fifoNumber) noexcept;
	bool getFifoPlSize(Fifo::Number fifoNumber) noexcept;
	bool getFifoEmptyFullFlag(Fifo::Number fifoNumber, bool& flag) noexcept;
	bool getFifoNotEmptyFullFlag(Fifo::Number fifoNumber, bool& flag) noexcept;
	bool getFifoPlSize(Fifo::Number fifoNumber, uint8_t& plSize) noexcept;

	static constexpr uint8_t Dlc2Bytes(uint8_t dlc) {
		//TODO in can2.0 mode max bytes qauntity is 8
		constexpr uint8_t sizeInBytes[16] {0, 1, 2, 3, 4, 5 ,6, 7, 8, 12, 16, 20, 24, 32, 48, 64};
		return sizeInBytes[dlc];
	}
};

} // namespace azn::Mcp2518
