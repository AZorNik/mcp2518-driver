#pragma once

#include <array>
#include "mcp2518-fifo.hpp"

namespace azn::mcp2518 {

	namespace Filter {
		namespace Control {
			inline constexpr auto FiltEn = BitField<uint8_t, bool>(1, 7);
			inline constexpr auto Fbp = BitField<uint8_t>(5, 0);
		};

		namespace Object {
			inline constexpr auto ExIdEn = BitField<uint32_t, bool>(1, 30);
			inline constexpr auto Sid11 = BitField<uint32_t>(1, 29);
			inline constexpr auto Eid = BitField<uint32_t>(18, 11);
			inline constexpr auto Sid = BitField<uint32_t>(11, 0);
			inline constexpr auto Id = BitField<uint32_t>(29, 0);
		};

		namespace Mask {
			inline constexpr auto MIdEn = BitField<uint32_t, bool>(1, 30);
			inline constexpr auto MSid11 = BitField<uint32_t>(1, 30);
			inline constexpr auto MEid = BitField<uint32_t>(18, 11);
			inline constexpr auto MSid = BitField<uint32_t>(11, 0);
			inline constexpr auto MId = BitField<uint32_t>(29, 0);
		};

		struct Registers_t {
			RegAddress control;
			RegAddress object;
			RegAddress mask;
		};

		inline constexpr uint8_t FiltersQty = 32;

		inline constexpr std::array<Registers_t, FiltersQty>Registers {[]{
			std::array<Registers_t, FiltersQty>filters{};
			constexpr uint16_t ConBaseAddr = 0x1d0;
			for (RegAddress i = 0; i < FiltersQty; i++) {
				filters[i].control = static_cast<RegAddress>(ConBaseAddr + i);
			}

			constexpr uint16_t ObjBaseAddr = 0x1f0;
			constexpr uint16_t MaskBaseAddr = 0x1f4;
			for (RegAddress i = 0; i < FiltersQty; i++) {
				filters[i].object = static_cast<RegAddress>(ObjBaseAddr + i * 8);
				filters[i].mask = static_cast<RegAddress>(MaskBaseAddr + i * 8);
			}

			return filters;
		}()};

		enum class TypeIdMatch {
			Standard,
			Extended,
			Both,
		};

		struct Config {
			TypeIdMatch typeId;
			uint32_t mask;
			uint32_t matchVal;
			Fifo::Number fifo;
		};
	} // namespace Filter
} // namespace azn::Mcp2518
