#pragma once
#include <cstdint>

namespace azn::mcp2518{

	struct BitTime {
		uint8_t prescaler;
		uint8_t tseg1;
		uint8_t tseg2;
		uint8_t sjw;
	};

	namespace NominalBitTime {
		inline constexpr uint8_t TSeg2Max = 127;
		inline constexpr uint8_t SjwMax = 127;
	};

	namespace DataBitTime {
		inline constexpr uint8_t TSeg1Max = 31;
		inline constexpr uint8_t TSeg2Max = 15;
		inline constexpr uint8_t SjwMax = 15;
	};
} // namespace azn::Mcp2518
