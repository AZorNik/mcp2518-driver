#include "mcp2518-driver.hpp"
#include "cstring"

namespace azn::mcp2518 {

template<typename T>
void frameToTransmitObj(const T& frame, TransmitMsgObj& obj) noexcept {
		TransmitMsgObj::Ide.setField(obj.val[1], frame.extId ? 1 : 0);
		TransmitMsgObj::Dlc.setField(obj.val[1], static_cast<uint8_t>(frame.dataSize));

		if (frame.extId) {
			TransmitMsgObj::Id.setField(obj.val[0], frame.id);
		}
		else {
			TransmitMsgObj::Sid.setField(obj.val[0], frame.id);
		}

		memcpy(reinterpret_cast<void*>(&obj.val[2]), reinterpret_cast<const void*>(&frame.data[0])
					 , can::dataSizeToBytesQty(frame.dataSize));
}

TransmitMsgObj canFrameToTransmitObj(const can::Can2Frame& frame) noexcept {
		TransmitMsgObj trObj;

		TransmitMsgObj::Rtr.setField(trObj.val[1], frame.rtr ? 1 : 0);

		frameToTransmitObj(frame, trObj);

		return trObj;
}

TransmitMsgObj canFdFrameToTransmitObj(const can::FdFrame& frame) noexcept {
		TransmitMsgObj trObj;

		TransmitMsgObj::Brs.setField(trObj.val[1], frame.brs);
		TransmitMsgObj::Fdf.setField(trObj.val[1], 1);

		frameToTransmitObj(frame, trObj);

		return trObj;
}

TransmitMsgObj frameToTransmitObj(const can::Frame& frame) noexcept {
		TransmitMsgObj obj;

		auto& header = frame.header;

		TransmitMsgObj::Ide.setField(obj.val[1], header.extId ? 1 : 0);
		TransmitMsgObj::Dlc.setField(obj.val[1], static_cast<uint8_t>(header.dataSize));

		if (header.extId) {
			TransmitMsgObj::Id.setField(obj.val[0], header.id);
		}
		else {
			TransmitMsgObj::Sid.setField(obj.val[0], header.id);
		}

		TransmitMsgObj::Fdf.setField(obj.val[1], header.fd);

		if (header.fd) {
			TransmitMsgObj::Brs.setField(obj.val[1], header.brs);
		}
		else {
			TransmitMsgObj::Rtr.setField(obj.val[1], header.rtr);
		}


		std::size_t neededDataSize = can::dataSizeToBytesQty(header.dataSize);

		auto bytesQty = std::min(frame.data.size(), neededDataSize);

		auto destAddr = reinterpret_cast<uint8_t*>(&obj.val[2]);
		for (std::size_t i = 0; i < bytesQty; ++i) {
			destAddr[i] = frame.data[i];
		}

		while(bytesQty < neededDataSize) {
			destAddr[bytesQty++] = 0;
		}

		return obj;
}

template<typename T>
void receiveObjToFrame(const ReceiveMsgObj& obj, T& frame) noexcept {
	frame.extId = obj.Eid.getField(obj.val[0]);
	frame.dataSize = static_cast<can::DataSize>(ReceiveMsgObj::Dlc.getField(obj.flags));

	if (frame.extId) {
		frame.id = ReceiveMsgObj::Id.getField(obj.id);
	}
	else {
		frame.id = ReceiveMsgObj::Sid.getField(obj.id);
	}

	memcpy(reinterpret_cast<void*>(&frame.data[0]), reinterpret_cast<const void*>(&obj.val[3])
			, can::dataSizeToBytesQty(frame.dataSize));
}

can::Can2Frame receiveObjToCanFrame(const ReceiveMsgObj& obj) noexcept {
	can::Can2Frame frame;
	frame.rtr = ReceiveMsgObj::Rtr.getField(obj.flags);
	receiveObjToFrame(obj, frame);

	return frame;
}

can::FdFrame receiveObjToCanFdFrame(const ReceiveMsgObj& obj) noexcept {
	can::FdFrame frame;
	frame.brs = ReceiveMsgObj::Brs.getField(obj.flags);
	receiveObjToFrame(obj, frame);

	return frame;
}

bool receiveObjToFrame(const ReceiveMsgObj& obj, can::Frame& frame) noexcept {
	auto& header = frame.header;

	header.extId = obj.Eid.getField(obj.val[0]);
	header.dataSize = static_cast<can::DataSize>(ReceiveMsgObj::Dlc.getField(obj.flags));

	if (header.extId) {
		header.id = ReceiveMsgObj::Id.getField(obj.id);
	}
	else {
		header.id = ReceiveMsgObj::Sid.getField(obj.id);
	}

	header.fd = ReceiveMsgObj::Fdf.getField(obj.flags);

	if (header.fd) {
		header.brs = ReceiveMsgObj::Brs.getField(obj.flags);
	}
	else {
		header.rtr = ReceiveMsgObj::Rtr.getField(obj.flags);
	}

	frame.data.clear();
	auto sourceAddr = reinterpret_cast<const uint8_t*>(&obj.val[3]);
	for (std::size_t i = 0; i < can::dataSizeToBytesQty(header.dataSize); ++i) {
		if (!frame.data.add(sourceAddr[i])) {
			break;
		}
	}

	return can::dataSizeToBytesQty(header.dataSize) == frame.data.size();
}

Driver::Driver(SpiRead read, SpiWrite write, SpiSelect select) noexcept
		: read(read)
		, write(write)
		, select(select)
		{

}

bool Driver::reset(void) noexcept {

	uint16_t init = byteswap(0x000) | (SpiCommand::Reset << AddressOffset);
	select(true);
	bool res = write(reinterpret_cast<uint8_t*>(&init), 2);
	select(false);

	return res;
}

bool Driver::readReg(RegAddress addr, uint8_t* buffer, uint32_t bytesQty) noexcept {
	uint16_t init = byteswap(addr) | (SpiCommand::Read << AddressOffset);
	select(true);
	bool res = write(reinterpret_cast<uint8_t*>(&init), 2);
	res = res && read(buffer, bytesQty);
	select(false);

	return res;
}

bool Driver::writeReg(RegAddress addr, const uint8_t* buffer, uint32_t bytesQty) noexcept {
	uint16_t init = byteswap(addr) | (SpiCommand::Write << AddressOffset);
	select(true);
	bool res = write(reinterpret_cast<uint8_t*>(&init), 2);
	res = res && write(buffer, bytesQty);
	select(false);

	return res;
}

bool Driver::readReg(Sfr reg, uint32_t& buffer) noexcept {
	return readReg(static_cast<RegAddress>(reg), reinterpret_cast<uint8_t*>(&buffer), 4);
}

bool Driver::writeReg(Sfr reg, const uint32_t& buffer) noexcept {
	return writeReg(static_cast<RegAddress>(reg), reinterpret_cast<const uint8_t*>(&buffer), 4);
}

bool Driver::readMsgMem(RegAddress addr, uint32_t* buffer, uint32_t wordsQty) noexcept {
	uint16_t init = byteswap(addr) | (SpiCommand::Read << AddressOffset);
	select(true);
	bool res = write(reinterpret_cast<uint8_t*>(&init), 2);
	res = res && read(reinterpret_cast<uint8_t*>(buffer), wordsQty * MsgMemWordSize);
	select(false);

	return res;
}

bool Driver::writeMsgMem(RegAddress addr, const uint32_t* buffer, uint32_t wordsQty) noexcept {
	uint16_t init = byteswap(addr) | (SpiCommand::Write << AddressOffset);
	select(true);
	bool res = write(reinterpret_cast<uint8_t*>(&init), 2);
	res = res && write(reinterpret_cast<const uint8_t*>(buffer), wordsQty * MsgMemWordSize);
	select(false);

	return res;
}

bool Driver::setFifoConfig(Fifo::Number fifoNumber, const Fifo::Config& config) noexcept {
	//TODO add checks

	bool ok = Fifo::isNumberOk(fifoNumber);
	ok = ok && config.fifoSize > 0;

	uint32_t regVal = 0;

	if (ok) {
		Fifo::Config::PlSize.setField(regVal, config.plSize);
		Fifo::Config::FifoSize.setField(regVal, config.fifoSize - 1);
		Fifo::Config::Txat.setField(regVal, config.retransmission);
		Fifo::Config::TxPriority.setField(regVal, config.priority);
		Fifo::Config::TxEn.setField(regVal, config.direction);
		Fifo::Config::RtrEn.setField(regVal, config.autoRtrEnable);
		Fifo::Config::RxtsEn.setField(regVal, config.receiveMsgTimeStampEnable);
		Fifo::Config::TxatiEn.setField(regVal, config.transmitExhaustedInterruptEnable);
		Fifo::Config::RxoviEn.setField(regVal, config.overflowInterruptEnable);
		Fifo::Config::TferffiEn.setField(regVal, config.emptyFullInterruptEnable);
		Fifo::Config::TfhrfhiEn.setField(regVal, config.halfEmptyFullInterruptEnable);
		Fifo::Config::TfnrfniEn.setField(regVal, config.notEmptyFullInterruptEnable);
	}

	if (ok) {
		auto regAddress = Fifo::Registers[fifoNumber - 1].con;
		ok = writeReg(regAddress, reinterpret_cast<uint8_t*>(&regVal), sizeof(regVal));
	}

	return ok;
}

bool Driver::getFifoConfig(Fifo::Number fifoNumber, Fifo::Config& config) noexcept {

	bool ok = Fifo::isNumberOk(fifoNumber);

	uint32_t regVal;

	if (ok) {
		auto regAddress = Fifo::Registers[fifoNumber - 1].con;

		ok = readReg(regAddress, reinterpret_cast<uint8_t*>(&regVal), sizeof(regVal));
	}

	if (ok) {
		config.plSize = Fifo::Config::PlSize.getField(regVal);
		config.fifoSize = Fifo::Config::FifoSize.getField(regVal) + 1;
		config.retransmission = Fifo::Config::Txat.getField(regVal);
		config.priority = Fifo::Config::TxPriority.getField(regVal);
		config.direction = Fifo::Config::TxEn.getField(regVal);
		config.autoRtrEnable = Fifo::Config::RtrEn.getField(regVal);
		config.receiveMsgTimeStampEnable = Fifo::Config::TxatiEn.getField(regVal);
		config.transmitExhaustedInterruptEnable = Fifo::Config::RxtsEn.getField(regVal);
		config.overflowInterruptEnable = Fifo::Config::RxoviEn.getField(regVal);
		config.emptyFullInterruptEnable = Fifo::Config::TferffiEn.getField(regVal);
		config.halfEmptyFullInterruptEnable = Fifo::Config::TfhrfhiEn.getField(regVal);
		config.notEmptyFullInterruptEnable = Fifo::Config::TfnrfniEn.getField(regVal);

	}

	return ok;
}

bool Driver::getFifoUserAddress(Fifo::Number fifoNumber, RegAddress& ua) noexcept {
	auto regAddress = Fifo::Registers[fifoNumber - 1].ua;
	bool ok = readReg(regAddress, reinterpret_cast<uint8_t*>(&ua), sizeof(ua));
	if (ok) {
		ua += MsgMemStart;
	}
	return ok;
}

bool Driver::incFifoUserAddress(Fifo::Number fifoNumber) noexcept {
	auto regAddress = Fifo::Registers[fifoNumber - 1].con;
	uint32_t val{0};
	Fifo::Config::Uinc.setField(val, 1);
	bool ok = writeReg(regAddress + 1, reinterpret_cast<uint8_t*>(&val) + 1, 1);
	return ok;
}

uint16_t Driver::bytesQty2WordsQty(uint16_t bytesQty) noexcept {
	auto wordsQty = bytesQty / MsgMemWordSize;
	if ((bytesQty % MsgMemWordSize) != 0) {
		wordsQty++;
	}

	return wordsQty;
}

bool Driver::getFifoEmptyFullFlag(Fifo::Number fifoNumber, bool& flag) noexcept {
	auto regAddress = Fifo::Registers[fifoNumber - 1].sta;
	uint32_t regValue;
	bool ok = readReg(regAddress, reinterpret_cast<uint8_t*>(&regValue), 1);
	if (ok) {
		flag = Fifo::Status::TfeRffIf.getField(regValue);
	}
	return ok;
}

bool Driver::getFifoNotEmptyFullFlag(Fifo::Number fifoNumber, bool& flag) noexcept {
	auto regAddress = Fifo::Registers[fifoNumber - 1].sta;
	uint32_t regValue;
	bool ok = readReg(regAddress, reinterpret_cast<uint8_t*>(&regValue), 1);
	if (ok) {
		flag = Fifo::Status::TfnRfnIf.getField(regValue);
	}
	return ok;
}

bool Driver::writeMsgObj(Fifo::Number fifoNumber, const TransmitMsgObj& obj) noexcept {

	bool ok = Fifo::isNumberOk(fifoNumber);

	if (ok) {
		bool notFull;
		ok = getFifoNotEmptyFullFlag(fifoNumber, notFull) && notFull;

		RegAddress userAddress;
		ok = ok && getFifoUserAddress(fifoNumber, userAddress);

		auto dlc = TransmitMsgObj::Dlc.getField(obj.val[1]);
		auto objSizeWord = TransmitMsgObjHeadSizeWord + bytesQty2WordsQty(dlc);

		ok = ok && writeMsgMem(userAddress, obj.val, objSizeWord);
		ok = ok && incFifoUserAddress(fifoNumber);
	}

	return ok;
}

bool Driver::getFifoPlSize(Fifo::Number fifoNumber, uint8_t& plSize ) noexcept {
	uint32_t regVal {0};
	bool ok = readReg(Fifo::Registers[fifoNumber - 1].con + 3, reinterpret_cast<uint8_t*>(&regVal) + 3, 1);

	plSize = Fifo::PlSize2Bytes(Fifo::Config::PlSize.getField(regVal));
	return ok;
}

bool Driver::readMsgObj(Fifo::Number fifoNumber, ReceiveMsgObj& obj) noexcept {

	bool ok = Fifo::isNumberOk(fifoNumber);

	if (ok) {
		bool notEmpty;
		ok = getFifoNotEmptyFullFlag(fifoNumber, notEmpty) && notEmpty;

		RegAddress userAddress;
		ok = ok && getFifoUserAddress(fifoNumber, userAddress);

		ok = ok && readMsgMem(userAddress, &obj.val[0], ReceiveMsgObjHeadSizeWord);
		auto dlc = ReceiveMsgObj::Dlc.getField(obj.val[1]);
		auto dataSizeWord = bytesQty2WordsQty(Dlc2Bytes(dlc));

		if (dataSizeWord > 0) {
			ok = ok && readMsgMem(userAddress + ReceiveMsgObjHeadSizeWord * MsgMemWordSize
					, &obj.val[3], dataSizeWord);
		}
		ok = ok && incFifoUserAddress(fifoNumber);
	}

	return ok;
}

bool Driver::setNominalBitTime(const BitTime& bitTime) noexcept {
	bool ok = (bitTime.tseg2 <= NominalBitTime::TSeg2Max) && (bitTime.sjw <= NominalBitTime::SjwMax);
	if (ok) {
		uint8_t buffer[4];
		buffer[0] = bitTime.sjw;
		buffer[1] = bitTime.tseg2;
		buffer[2] = bitTime.tseg1;
		buffer[3] = bitTime.prescaler;

		ok = writeReg(static_cast<RegAddress>(Sfr::Nbtcfg), &buffer[0], 4);
	}
	return ok;
}

bool Driver::setDataBitTime(const BitTime& bitTime) noexcept {
	bool ok = (bitTime.tseg1 <= DataBitTime::TSeg1Max) && (bitTime.tseg2 <= DataBitTime::TSeg2Max)
			&& (bitTime.sjw <= DataBitTime::SjwMax);

	if (ok) {
		uint8_t buffer[4];
		buffer[0] = bitTime.sjw;
		buffer[1] = bitTime.tseg2;
		buffer[2] = bitTime.tseg1;
		buffer[3] = bitTime.prescaler;

		ok = writeReg(static_cast<RegAddress>(Sfr::Dbtcfg), &buffer[0], 4);
	}
	return ok;
}

bool Driver::getNominalBitTime(BitTime& bitTime) noexcept {
	uint8_t buffer[4];
	bool ok = readReg(static_cast<RegAddress>(Sfr::Nbtcfg), &buffer[0], 4);

	if (ok) {
		bitTime.sjw = buffer[0];
		bitTime.tseg2 = buffer[1];
		bitTime.tseg1 = buffer[2];
		bitTime.prescaler = buffer[3];
	}

	return ok;
}

bool Driver::getDataBitTime(BitTime& bitTime) noexcept {
	uint8_t buffer[4];
	bool ok = readReg(static_cast<RegAddress>(Sfr::Dbtcfg), &buffer[0], 4);

	if (ok) {
		bitTime.sjw = buffer[0];
		bitTime.tseg2 = buffer[1];
		bitTime.tseg1 = buffer[2];
		bitTime.prescaler = buffer[3];
	}

	return ok;
}

bool Driver::msgSendRequest(Fifo::Number fifoNumber) noexcept {
	uint32_t regVal = 0;
	Fifo::Config::TxReq.setField(regVal, 1);
	bool ok = writeReg(Fifo::Registers[fifoNumber - 1].con + 1, reinterpret_cast<uint8_t*>(&regVal) + 1, 1);

	return ok;
}
bool Driver::loadFrame(Fifo::Number fifoNumber, const can::Can2Frame& frame) noexcept {
	bool ok = frame.dataSize <= can::DataSize::Size_8;
	ok = ok && writeMsgObj(fifoNumber, canFrameToTransmitObj(frame));

	return ok;
}

bool Driver::sendFrame(Fifo::Number fifoNumber, const can::Can2Frame& frame) noexcept {
	bool ok = loadFrame(fifoNumber, frame);
	ok = ok && msgSendRequest(fifoNumber);

	return ok;
}

bool Driver::receiveFrame(Fifo::Number fifoNumber, can::Can2Frame& frame) noexcept {
	ReceiveMsgObj obj;
	bool ok = readMsgObj(fifoNumber, obj);

	frame = receiveObjToCanFrame(obj);

	return ok;
}

bool Driver::loadFrame(Fifo::Number fifoNumber, const can::FdFrame& frame) noexcept {
	bool ok = writeMsgObj(fifoNumber, canFdFrameToTransmitObj(frame));
	return ok;
}

bool Driver::sendFrame(Fifo::Number fifoNumber, const can::FdFrame& frame) noexcept {
	bool ok = loadFrame(fifoNumber, frame);
	ok = ok && msgSendRequest(fifoNumber);

	return ok;
}

bool Driver::receiveFrame(Fifo::Number fifoNumber, can::FdFrame& frame) noexcept {
	ReceiveMsgObj obj;
	bool ok = readMsgObj(fifoNumber, obj);

	frame = receiveObjToCanFdFrame(obj);

	return ok;
}

bool Driver::loadFrame(Fifo::Number fifoNumber, const can::Frame& frame) noexcept {
	bool ok = (frame.header.fd != false) || (frame.header.dataSize <= can::DataSize::Size_8);
	ok = ok && writeMsgObj(fifoNumber, frameToTransmitObj(frame));

	return ok;
}

bool Driver::sendFrame(Fifo::Number fifoNumber, const can::Frame& frame) noexcept {
	bool ok = loadFrame(fifoNumber, frame);
	ok = ok && msgSendRequest(fifoNumber);

	return ok;
}

bool Driver::receiveFrame(Fifo::Number fifoNumber, can::Frame& frame) noexcept {
	ReceiveMsgObj obj;
	bool ok = readMsgObj(fifoNumber, obj);
	ok = ok && receiveObjToFrame(obj, frame);
	return ok;
}

bool Driver::filterSetEnable(uint8_t filterNumber, bool enable) noexcept {
	uint8_t reg;
	bool ok = filterNumber < Filter::FiltersQty;
	ok = ok && readReg(Filter::Registers[filterNumber].control, &reg, 1);
	Filter::Control::FiltEn.setField(reg, enable);

	ok = ok && writeReg(Filter::Registers[filterNumber].control, &reg, 1);

	return ok;
}

bool Driver::filterSetConfig(uint8_t filterNumber, const Filter::Config& config) noexcept {
	uint32_t regs[2] {0};
	Filter::Object::ExIdEn.setField(regs[0], config.typeId == Filter::TypeIdMatch::Extended);
	Filter::Mask::MIdEn.setField(regs[1], config.typeId != Filter::TypeIdMatch::Both);


	if (config.typeId == Filter::TypeIdMatch::Standard){
		Filter::Object::Sid.setField(regs[0], config.matchVal);
		Filter::Mask::MSid.setField(regs[1], config.mask);
	}
	else {
		Filter::Object::Id.setField(regs[0], config.matchVal);
		Filter::Mask::MId.setField(regs[1], config.mask);
	}

	bool ok = filterNumber < Filter::FiltersQty;
	ok = ok && writeReg(Filter::Registers[filterNumber].object, reinterpret_cast<uint8_t*>(&regs), 8);

	uint8_t reg;
	ok = ok && readReg(Filter::Registers[filterNumber].control, &reg, 1);
	Filter::Control::Fbp.setField(reg, config.fifo);
	ok = ok && writeReg(Filter::Registers[filterNumber].control, &reg, 1);

	return ok;
}

bool Driver::filterGetConfig(uint8_t filterNumber, Filter::Config& config) noexcept {
	uint32_t regs[2] {0};
	bool ok = filterNumber < Filter::FiltersQty;
	ok = ok && readReg(Filter::Registers[filterNumber].object, reinterpret_cast<uint8_t*>(&regs), 8);

	config.matchVal = Filter::Object::Id.getField(regs[0]);
	config.mask = Filter::Mask::MId.getField(regs[1]);

	if (!Filter::Mask::MIdEn.getField(regs[1])) {
		config.typeId = Filter::TypeIdMatch::Both;
	}
	else {
		auto eIdEn = Filter::Object::ExIdEn.getField(regs[0]);
		config.typeId = eIdEn ? Filter::TypeIdMatch::Extended : Filter::TypeIdMatch::Standard;
	}

	return ok;
}

bool Driver::setOscConfig(const Osc::Config& config) noexcept {
	uint32_t reg {0};
	Osc::ClkODiv.setField(reg, config.outputDiv);
	Osc::SClkDiv.setField(reg, config.systemClkDiv);
	Osc::LpmEn.setField(reg, config.lowPowerModeEn);
	Osc::PllEn.setField(reg, config.pllEn);

	bool ok = writeReg(Sfr::Osc, reg);
	return ok;
}

bool Driver::getOscConfig(Osc::Config& config) noexcept {
	uint32_t reg;
	bool ok = readReg(Sfr::Osc, reg);

	if (ok) {
		config.outputDiv = Osc::ClkODiv.getField(reg);
		config.systemClkDiv = Osc::SClkDiv.getField(reg);
		config.lowPowerModeEn = Osc::LpmEn.getField(reg);
		config.pllEn = Osc::PllEn.getField(reg);
	}

	return ok;
}

bool Driver::getOscFlags(Osc::Flags& flags) noexcept {
	uint32_t reg;
	bool ok = readReg(Sfr::Osc, reg);

	if (ok) {
		flags.sysClkReady = Osc::SclkRdy.getField(reg);
		flags.oscReady = Osc::OscRdy.getField(reg);
		flags.pllReady = Osc::PllRdy.getField(reg);
	}

	return ok;

}

bool Driver::requestOpMod(Control::OperationMode mode) noexcept {
	bool ok;
	uint32_t reg;
	ok = readReg(Sfr::Con, reg);
	Control::Reqop.setField(reg, mode);
	ok = ok && writeReg(Sfr::Con, reg);
	return ok;
}

bool Driver::getOpMod(Control::OperationMode& mode) noexcept {
	bool ok;
	uint32_t reg;
	ok = readReg(Sfr::Con, reg);
	mode = Control::Opmode.getField(reg);
	return ok;
}

bool Driver::ioSetConfig(const Io::Config& config) noexcept {
	uint32_t reg = 0;
	bool ok = readReg(Sfr::Iocon, reg);

	Io::IntOd.setField(reg, config.interruptPinsMode);
	Io::Sof.setField(reg, config.clkoPinFunction);
	Io::TxCanOd.setField(reg, config.txPinMode);
	Io::Pm1.setField(reg, config.pin1Function);
	Io::Pm0.setField(reg, config.pin0Function);
	Io::XstbyEn.setField(reg, config.autoTransceiverStandbyEn);
	Io::Tris1.setField(reg, config.pin1Direction);
	Io::Tris0.setField(reg, config.pin0Direction);

	ok = ok && writeReg(Sfr::Iocon, reg);

	return ok;
}

bool Driver::ioGetConfig(Io::Config& config) noexcept {
	uint32_t reg = 0;
	bool ok = readReg(Sfr::Iocon, reg);

	if (ok) {
		config.interruptPinsMode = Io::IntOd.getField(reg);
		config.clkoPinFunction = Io::Sof.getField(reg);
		config.txPinMode = Io::TxCanOd.getField(reg);
		config.pin1Function = Io::Pm1.getField(reg);
		config.pin0Function = Io::Pm0.getField(reg);
		config.autoTransceiverStandbyEn = Io::XstbyEn.getField(reg);
		config.pin1Direction = Io::Tris1.getField(reg);
		config.pin0Direction = Io::Tris0.getField(reg);
	}

	return ok;
}

bool Driver::ioSetPinState(Io::PinId pin, Io::PinState state) noexcept {
	uint32_t reg = 0;
	bool ok = readReg(Sfr::Iocon, reg);

	if (pin == Io::PinId::Pin0) {
		Io::Lat0.setField(reg, state);
	}
	else if (pin == Io::PinId::Pin1) {
		Io::Lat1.setField(reg, state);
	}

	ok =  ok && writeReg(Sfr::Iocon, reg);

	return ok;
}

bool Driver::ioGetPinState(Io::PinId pin, Io::PinState& state) noexcept {
	uint32_t reg = 0;
	bool ok = readReg(Sfr::Iocon, reg);

	if (pin == Io::PinId::Pin0) {
		state = Io::Gpio0.getField(reg);
	}
	else {
		state = Io::Gpio1.getField(reg);
	}

	return ok;
}


bool Driver::interruptGetCodes(Interrupt::Codes& codes) noexcept {
	uint32_t reg;
	bool ok = readReg(Sfr::Con, reg);

	if (ok) {
		codes.Rx = Interrupt::Codes::RxMask.getField(reg);
		codes.Tx = Interrupt::Codes::TxMask.getField(reg);
		codes.Filter = Interrupt::Codes::FilterMask.getField(reg);
		codes.ICode = Interrupt::Codes::ICodeMask.getField(reg);
	}

	return ok;
}

bool Driver::interruptEnable(Interrupt::Main interrupt) noexcept {
	uint32_t reg;
	bool ok = readReg(Sfr::Int, reg);

	if (ok) {
		reg |= static_cast<uint16_t>(interrupt) << 16;
		reg |= 0xffff;
		ok = writeReg(Sfr::Int, reg);
	}

	return ok;
}

bool Driver::interruptDisable(Interrupt::Main interrupt) noexcept {
	uint32_t reg;
	bool ok = readReg(Sfr::Int, reg);

	if (ok) {
		reg &= ~(static_cast<uint16_t>(interrupt) << 16);
		reg |= 0xffff;
		ok = writeReg(Sfr::Int, reg);
	}

	return ok;
}

bool Driver::interruptEnable(Interrupt::Group interrupts) noexcept {
	uint32_t reg;
	bool ok = readReg(Sfr::Int, reg);

	if (ok) {
		reg |= interrupts.flags << 16;
		reg |= 0xffff;
		ok = writeReg(Sfr::Int, reg);
	}

	return ok;
}

bool Driver::interruptDisable(Interrupt::Group interrupts) noexcept {
	uint32_t reg;
	bool ok = readReg(Sfr::Int, reg);

	if (ok) {
		reg &= ~(interrupts.flags << 16);
		reg |= 0xffff;
		ok = writeReg(Sfr::Int, reg);
	}

	return ok;
}

bool Driver::interruptEnableAll(void) noexcept {
	uint32_t reg = 0xff1fffff;
	bool ok = writeReg(Sfr::Int, reg);

	return ok;
}

bool Driver::interruptDisableAll(void) noexcept {
	uint32_t reg = 0xffff;
	bool ok = writeReg(Sfr::Int, reg);

	return ok;
}

bool Driver::interruptGetFlags(Interrupt::Group& interrupts) noexcept {
	uint32_t reg;
	bool ok = readReg(Sfr::Int, reg);

	if (ok) {
		interrupts.flags = reg;
	}

	return ok;
}

bool Driver::interruptGetRxStatus(uint32_t& status) noexcept {
	bool ok = readReg(Sfr::Rxif, status);
	return ok;
}

bool Driver::interruptGetOverflowStatus(uint32_t& status) noexcept {
	bool ok = readReg(Sfr::Rxovif, status);
	return ok;
}

bool Driver::interruptGetTxStatus(uint32_t& status) noexcept {
	bool ok = readReg(Sfr::Txif, status);
	return ok;
}

bool Driver::interruptGetTxAttemptStatus(uint32_t& status) noexcept {
	bool ok = readReg(Sfr::Txatif, status);
	return ok;
}


bool Driver::errorsGetCounters(Errors::Trec& errors) noexcept {
	uint32_t reg;
	bool ok = readReg(Sfr::Trec, reg);
	if	(ok) {
		errors.busOff = Errors::Txbo.getField(reg);
		errors.txPassive = Errors::Txbp.getField(reg);
		errors.rxPassive = Errors::Rxbp.getField(reg);
		errors.txWarning = Errors::Txwarn.getField(reg);
		errors.rxWarning = Errors::Rxwarn.getField(reg);
		errors.warning = Errors::Ewarn.getField(reg);
		errors.txErrorCounter = Errors::Tec.getField(reg);
		errors.rxErrorCounter = Errors::Rec.getField(reg);
	}

	return ok;
}

} //namespce azn::Mcp2518
