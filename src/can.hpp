#pragma once
#include <cstdint>
#include "azn/cpp_lib/array.hpp"

namespace azn::can {

enum class DataSize : uint8_t {
	Size_0 = 0,
	Size_1 = 1,
	Size_2 = 2,
	Size_3 = 3,
	Size_4 = 4,
	Size_5 = 5,
	Size_6 = 6,
	Size_7 = 7,
	Size_8 = 8,
	Size_12 = 9,
	Size_16 = 10,
	Size_20 = 11,
	Size_24 = 12,
	Size_32 = 13,
	Size_48 = 14,
	Size_64 = 15
};

inline constexpr uint8_t dataSizeToBytesQty(DataSize size) {
	constexpr uint8_t sizeInBytes[16] {0, 1, 2, 3, 4, 5 ,6, 7, 8, 12, 16, 20, 24, 32, 48, 64};
	return sizeInBytes[static_cast<uint8_t>(size)];
}

struct FrameHeader {
	uint32_t id;
	DataSize dataSize;
	bool extId;
	bool fd;
	bool rtr; // only for can 2.0 frame
	bool brs; // only for fd frame
};

struct Frame {
	Frame(uint8_t* buf, std::size_t bufSize)
			: data{buf, bufSize}
		{
	}

	FrameHeader header{0, DataSize::Size_0, false, false, false, false};
	azn::Array<uint8_t> data;
};

struct Can2Frame {
	uint32_t id;
	uint8_t data[8];
	DataSize dataSize {DataSize::Size_0};
	bool extId;
	bool rtr;
};

struct FdFrame {
	uint32_t id;
	uint8_t data[64];
	DataSize dataSize {DataSize::Size_0};
	bool extId;
	bool brs;
};

}
