#pragma once
#include <cstdint>
#include "bitField.hpp"
#include "can.hpp"


namespace azn::mcp2518{

	using RegAddress = uint16_t;

	enum class Sfr : RegAddress {
		Con = 0x0,
		Nbtcfg = 0x4,
		Dbtcfg = 0x8,
		Tdc = 0xC,
		Tbc = 0x10,
		Tscon = 0x14,
		Vec = 0x18,
		Int = 0x1C,
		Rxif = 0x20,
		Txif = 0x24,
		Rxovif = 0x28,
		Txatif = 0x2C,
		Txreq = 0x30,
		Trec = 0x34,
		Bdiag0 = 0x38,
		Bdiag1 = 0x3C,
		Tefcon = 0x40,
		Tefsta = 0x44,
		Tefua = 0x48,
		Txqcon = 0x50,
		Txqsta = 0x54,
		Txqua = 0x58,

		Osc = 0xE00,
		Iocon = 0xE04,
		Crc = 0xE08,
		Ecccon = 0xE0C,
		Eccstat = 0xE10,
		DevId = 0xE14,
	};


	inline constexpr uint8_t TransmitMsgObjHeadSizeWord = 2;
	inline constexpr uint8_t ReceiveMsgObjHeadSizeWord = 3;
	inline constexpr uint8_t MsgObjDataSizeWord = 16;

	struct TransmitMsgObj {
		uint32_t val[TransmitMsgObjHeadSizeWord + MsgObjDataSizeWord] {0};

		static constexpr auto Sid11 = BitField<uint32_t>(1, 29);
		static constexpr auto Eid = BitField<uint32_t>(18, 11);
		static constexpr auto Sid = BitField<uint32_t>(11, 0);
		static constexpr auto Id = BitField<uint32_t>(29, 0);

		static constexpr auto Seq = BitField<uint32_t>(23, 9);
		static constexpr auto Esi = BitField<uint32_t, bool>(1, 8);
		static constexpr auto Fdf = BitField<uint32_t, bool>(1, 7);
		static constexpr auto Brs = BitField<uint32_t, bool>(1, 6);
		static constexpr auto Rtr = BitField<uint32_t, bool>(1, 5);
		static constexpr auto Ide = BitField<uint32_t, bool>(1, 4);
		static constexpr auto Dlc = BitField<uint32_t>(4, 0);
	};

	struct ReceiveMsgObj {
		uint32_t val[ReceiveMsgObjHeadSizeWord + MsgObjDataSizeWord] {0};
		uint32_t& id = val[0];
		uint32_t& flags = val[1];

		static constexpr auto Sid11 = BitField<uint32_t>(1, 29);
		static constexpr auto Eid = BitField<uint32_t>(18, 11);
		static constexpr auto Sid = BitField<uint32_t>(11, 0);
		static constexpr auto Id = BitField<uint32_t>(29, 0);

		static constexpr auto FilHit = BitField<uint32_t>(5, 11);
		static constexpr auto Esi = BitField<uint32_t, bool>(1, 8);
		static constexpr auto Fdf = BitField<uint32_t, bool>(1, 7);
		static constexpr auto Brs = BitField<uint32_t, bool>(1, 6);
		static constexpr auto Rtr = BitField<uint32_t, bool>(1, 5);
		static constexpr auto Ide = BitField<uint32_t, bool>(1, 4);
		static constexpr auto Dlc = BitField<uint32_t>(4, 0);
	};


	TransmitMsgObj canFrameToTransmitObj(const can::Can2Frame& frame) noexcept;
	can::Can2Frame receiveObjToCanFrame(const ReceiveMsgObj& obj) noexcept;
	TransmitMsgObj canFdFrameToTransmitObj(const can::FdFrame& frame) noexcept;
	can::FdFrame receiveObjToCanFdFrame(const ReceiveMsgObj& obj) noexcept;

	TransmitMsgObj frameToTransmitObj(const can::Frame& frame) noexcept;
	bool receiveObjToFrame(const ReceiveMsgObj& obj, can::Frame& frame) noexcept;

	namespace Control {
		enum class OperationMode : uint32_t {
			CanFd = 0,
			Sleep = 1,
			InteranlLoopBack = 2,
			ListenOnly = 3,
			Configuration = 4,
			ExternalLoopback = 5,
			Can2 = 6,
			Restricted = 7,
		};

		enum class TransmissionDelay : uint32_t {
			Bit0 = 0,
			Bit2 = 1,
			Bit4 = 2,
			Bit8 = 3,
			Bit16 = 4,
			Bit32 = 5,
			Bit64 = 6,
			Bit128 = 7,
			Bit256 = 8,
			Bit512 = 9,
			Bit1024 = 10,
			Bit2048 = 11,
			Bit4096 = 12,
		};

		inline constexpr auto Txbws = BitField<uint32_t, TransmissionDelay>(4, 28);
		inline constexpr auto Abat = BitField<uint32_t>(1, 27);
		inline constexpr auto Reqop = BitField<uint32_t, OperationMode>(3, 24);
		inline constexpr auto Opmode = BitField<uint32_t, OperationMode>(3, 21);
		inline constexpr auto Txqen = BitField<uint32_t>(1, 20);
		inline constexpr auto Stef = BitField<uint32_t>(1, 19);
		inline constexpr auto Serr2lom = BitField<uint32_t>(1, 18);
		inline constexpr auto Esigm = BitField<uint32_t>(1, 17);
		inline constexpr auto Ttxat = BitField<uint32_t>(1, 16);
		inline constexpr auto Brsdis = BitField<uint32_t>(1, 12);
		inline constexpr auto Busy = BitField<uint32_t>(1, 11);
		inline constexpr auto Wft = BitField<uint32_t>(2, 9);
		inline constexpr auto Wakfil = BitField<uint32_t>(1, 8);
		inline constexpr auto Pxedis = BitField<uint32_t>(1, 6);
		inline constexpr auto Isocrcen = BitField<uint32_t>(1, 5);
		inline constexpr auto Dncnt = BitField<uint32_t>(5, 0);
	}

// register Osc
	namespace Osc {
		enum class OutputDivisor : uint8_t {
			Div1 = 0,
			Div2 = 1,
			Div4 = 2,
			Div10 = 3,
		};

		enum class SystemClkDivisor : uint8_t {
			Div1 = 0,
			Div2 = 1,
		};

		struct Config {
			OutputDivisor outputDiv {OutputDivisor::Div10};
			SystemClkDivisor systemClkDiv {SystemClkDivisor::Div1};
			bool lowPowerModeEn {false};
			bool pllEn {false};
		};

		struct Flags {
			bool sysClkReady;
			bool oscReady;
			bool pllReady;
		};

		inline constexpr auto SclkRdy = BitField<uint32_t, bool>(1, 12);
		inline constexpr auto OscRdy = BitField<uint32_t, bool>(1, 10);
		inline constexpr auto PllRdy = BitField<uint32_t, bool>(1, 8);
		inline constexpr auto ClkODiv = BitField<uint32_t, OutputDivisor>(2, 5);
		inline constexpr auto SClkDiv = BitField<uint32_t, SystemClkDivisor>(1, 4);
		inline constexpr auto LpmEn = BitField<uint32_t, bool>(1, 3);
		inline constexpr auto OscDis = BitField<uint32_t, bool>(1, 2);
		inline constexpr auto PllEn = BitField<uint32_t, bool>(1, 0);
	}

	namespace Io {
		enum class PinMode : uint8_t {
			PushPull = 0,
			OpenDrain = 1,
		};

		enum class ClkoPinFunction : uint8_t {
			Clock = 0,
			StartOfFrame = 1,
		};

		enum class PinFunction : uint8_t {
			Interrrupt = 0,
			Gpio = 1,
		};

		enum class PinDirection : uint8_t {
			Output = 0,
			Input = 1,
		};

		enum class PinState : uint8_t {
			Reset = 0,
			Set = 1,
		};

		enum class PinId : uint8_t {
			Pin0,
			Pin1,
		};

		struct Config {
			PinMode interruptPinsMode;
			PinMode txPinMode;
			ClkoPinFunction clkoPinFunction;
			PinFunction pin0Function;
			PinDirection pin0Direction;
			PinFunction pin1Function;
			PinDirection pin1Direction;
			bool autoTransceiverStandbyEn;
		};

		inline constexpr auto IntOd = BitField<uint32_t, PinMode>(1, 30);
		inline constexpr auto Sof = BitField<uint32_t, ClkoPinFunction>(1, 29);
		inline constexpr auto TxCanOd = BitField<uint32_t, PinMode>(1, 28);
		inline constexpr auto Pm1 = BitField<uint32_t, PinFunction>(1, 25);
		inline constexpr auto Pm0 = BitField<uint32_t, PinFunction>(1, 24);
		inline constexpr auto Gpio1 = BitField<uint32_t, PinState>(1, 17);
		inline constexpr auto Gpio0 = BitField<uint32_t, PinState>(1, 16);
		inline constexpr auto Lat1 = BitField<uint32_t, PinState>(1, 9);
		inline constexpr auto Lat0 = BitField<uint32_t, PinState>(1, 8);
		inline constexpr auto XstbyEn = BitField<uint32_t>(1, 6);
		inline constexpr auto Tris1 = BitField<uint32_t, PinDirection>(1, 1);
		inline constexpr auto Tris0 = BitField<uint32_t, PinDirection>(1, 0);
	} // namespace Io

	namespace Errors {
		struct Trec {
			uint8_t txErrorCounter;
			uint8_t rxErrorCounter;
			bool warning;
			bool txWarning;
			bool rxWarning;
			bool txPassive;
			bool rxPassive;
			bool busOff;
		};

		inline constexpr auto Txbo = BitField<uint32_t, bool>(1, 21);
		inline constexpr auto Txbp = BitField<uint32_t, bool>(1, 20);
		inline constexpr auto Rxbp = BitField<uint32_t, bool>(1, 19);
		inline constexpr auto Txwarn = BitField<uint32_t, bool>(1, 18);
		inline constexpr auto Rxwarn = BitField<uint32_t, bool>(1, 17);
		inline constexpr auto Ewarn = BitField<uint32_t, bool>(1, 16);
		inline constexpr auto Tec = BitField<uint32_t>(8, 8);
		inline constexpr auto Rec = BitField<uint32_t>(8, 0);

	} // namespace Errors

// register DevId
	static constexpr auto DevId_id = BitField<uint32_t>(4, 4);
	static constexpr auto DevId_Rev = BitField<uint32_t>(4, 0);

} // namespace azn::Mcp2518
