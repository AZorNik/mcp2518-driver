#include "gtest/gtest.h"
#include "mcp2518.hpp"
#include "pseudoSpi.hpp"

using namespace azn::mcp2518;

static std::ostringstream ostr;
static PseudoSpi pseudoSpi(ostr);
static Driver mcp([](uint8_t* buffer, uint8_t qty) { return pseudoSpi.read(buffer, qty); }
		, [](const uint8_t* buffer, uint8_t qty) { return pseudoSpi.write(buffer, qty); }
		, [](bool cs){ return pseudoSpi.select(cs); }
);

using Spi = PseudoSpi;

TEST(filter, enable_0) {
	ostr.str("");
	pseudoSpi.reset();
	bool ok = mcp.filterSetEnable(0, true);

	std::string expected = Spi::strSelecet()
			+ Spi::strWrite({0x31, 0xd0}) + Spi::strRead({0x00})
			+ Spi::strUnselecet();
	expected += Spi::strSelecet()
			+ Spi::strWrite({0x21, 0xd0})	+ Spi::strWrite({0x80})
			+ Spi::strUnselecet();

	EXPECT_TRUE(ok);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(filter, enable_31) {
	ostr.str("");
	pseudoSpi.reset();
	bool ok = mcp.filterSetEnable(31, true);

	std::string expected = Spi::strSelecet()
			+ Spi::strWrite({0x31, 0xef}) + Spi::strRead({0x00})
			+ Spi::strUnselecet();
	expected += Spi::strSelecet()
			+ Spi::strWrite({0x21, 0xef})	+ Spi::strWrite({0x80})
			+ Spi::strUnselecet();

	EXPECT_TRUE(ok);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(filter, disable_0) {
	ostr.str("");
	pseudoSpi.reset();
	bool ok = mcp.filterSetEnable(0, false);

	std::string expected = Spi::strSelecet()
			+ Spi::strWrite({0x31, 0xd0}) + Spi::strRead({0x00})
			+ Spi::strUnselecet();
	expected += Spi::strSelecet()
			+ Spi::strWrite({0x21, 0xd0})	+ Spi::strWrite({0x00})
			+ Spi::strUnselecet();

	EXPECT_TRUE(ok);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(filter, disable_31) {
	ostr.str("");
	pseudoSpi.reset();
	bool ok = mcp.filterSetEnable(31, false);

	std::string expected = Spi::strSelecet()
			+ Spi::strWrite({0x31, 0xef}) + Spi::strRead({0x00})
			+ Spi::strUnselecet();
	expected += Spi::strSelecet()
			+ Spi::strWrite({0x21, 0xef})	+ Spi::strWrite({0x00})
			+ Spi::strUnselecet();

	EXPECT_TRUE(ok);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(filter, enable_32) {
	ostr.str("");
	pseudoSpi.reset();
	bool ok = mcp.filterSetEnable(32, true);

	EXPECT_FALSE(ok);
}

TEST(filter, setConfig_Sid) {
	ostr.str("");
	pseudoSpi.reset();

	Filter::Config config;
	config.fifo = 0;
	config.typeId = Filter::TypeIdMatch::Standard;
	config.matchVal = 0;
	config.mask = 0;

	bool ok = mcp.filterSetConfig(0, config);

	using spi = PseudoSpi;
	std::string expected = spi::strSelecet()
			+ spi::strWrite({0x21, 0xf0})	+ spi::strWrite({0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40})
			+ spi::strUnselecet();
	expected += spi::strSelecet()
			+ spi::strWrite({0x31, 0xD0}) + spi::strRead({0x00})
			+ spi::strUnselecet();
	expected += spi::strSelecet()
			+ spi::strWrite({0x21, 0xD0}) + spi::strWrite({0x00})
			+ spi::strUnselecet();

	EXPECT_TRUE(ok);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(filter, setConfig_Eid) {
	ostr.str("");
	pseudoSpi.reset();

	Filter::Config config;
	config.fifo = 0;
	config.typeId = Filter::TypeIdMatch::Extended;
	config.matchVal = 0;
	config.mask = 0;

	bool ok = mcp.filterSetConfig(0, config);

	using spi = PseudoSpi;
	std::string expected = spi::strSelecet()
			+ spi::strWrite({0x21, 0xf0})	+ spi::strWrite({0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x40})
			+ spi::strUnselecet();
	expected += spi::strSelecet()
			+ spi::strWrite({0x31, 0xD0}) + spi::strRead({0x00})
			+ spi::strUnselecet();
	expected += spi::strSelecet()
			+ spi::strWrite({0x21, 0xD0}) + spi::strWrite({0x00})
			+ spi::strUnselecet();

	EXPECT_TRUE(ok);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(filter, setConfig_Both_id) {
	ostr.str("");
	pseudoSpi.reset();

	Filter::Config config;
	config.fifo = 0;
	config.typeId = Filter::TypeIdMatch::Both;
	config.matchVal = 0;
	config.mask = 0;

	bool ok = mcp.filterSetConfig(0, config);

	using spi = PseudoSpi;
	std::string expected = spi::strSelecet()
			+ spi::strWrite({0x21, 0xf0})	+ spi::strWrite({0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00})
			+ spi::strUnselecet();
	expected += spi::strSelecet()
			+ spi::strWrite({0x31, 0xD0}) + spi::strRead({0x00})
			+ spi::strUnselecet();
	expected += spi::strSelecet()
			+ spi::strWrite({0x21, 0xD0}) + spi::strWrite({0x00})
			+ spi::strUnselecet();

	EXPECT_TRUE(ok);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(filter, setConfig_fifo0) {
	ostr.str("");
	pseudoSpi.reset();

	Filter::Config config;
	config.fifo = 0;
	config.typeId = Filter::TypeIdMatch::Extended;
	config.matchVal = 0;
	config.mask = 0;

	bool ok = mcp.filterSetConfig(0, config);

	using spi = PseudoSpi;
	std::string expected = spi::strSelecet()
			+ spi::strWrite({0x21, 0xf0})	+ spi::strWrite({0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x40})
			+ spi::strUnselecet();
	expected += spi::strSelecet()
			+ spi::strWrite({0x31, 0xD0}) + spi::strRead({0x00})
			+ spi::strUnselecet();
	expected += spi::strSelecet()
			+ spi::strWrite({0x21, 0xD0}) + spi::strWrite({0x00})
			+ spi::strUnselecet();

	EXPECT_TRUE(ok);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(filter, setConfig_fifo1) {
	ostr.str("");
	pseudoSpi.reset();

	Filter::Config config;
	config.fifo = 1;
	config.typeId = Filter::TypeIdMatch::Extended;
	config.matchVal = 0;
	config.mask = 0;

	bool ok = mcp.filterSetConfig(0, config);

	using spi = PseudoSpi;
	std::string expected = spi::strSelecet()
			+ spi::strWrite({0x21, 0xf0})	+ spi::strWrite({0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x40})
			+ spi::strUnselecet();
	expected += spi::strSelecet()
			+ spi::strWrite({0x31, 0xD0}) + spi::strRead({0x00})
			+ spi::strUnselecet();
	expected += spi::strSelecet()
			+ spi::strWrite({0x21, 0xD0}) + spi::strWrite({0x01})
			+ spi::strUnselecet();

	EXPECT_TRUE(ok);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(filter, setConfig_fifo31) {
	ostr.str("");
	pseudoSpi.reset();

	Filter::Config config;
	config.fifo = 31;
	config.typeId = Filter::TypeIdMatch::Extended;
	config.matchVal = 0;
	config.mask = 0;

	bool ok = mcp.filterSetConfig(0, config);

	using spi = PseudoSpi;
	std::string expected = spi::strSelecet()
			+ spi::strWrite({0x21, 0xf0})	+ spi::strWrite({0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x40})
			+ spi::strUnselecet();
	expected += spi::strSelecet()
			+ spi::strWrite({0x31, 0xD0}) + spi::strRead({0x00})
			+ spi::strUnselecet();
	expected += spi::strSelecet()
			+ spi::strWrite({0x21, 0xD0}) + spi::strWrite({0x1f})
			+ spi::strUnselecet();

	EXPECT_TRUE(ok);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(filter, setConfig_fifo32) {
	ostr.str("");
	pseudoSpi.reset();

	Filter::Config config;
	config.fifo = 32;
	config.typeId = Filter::TypeIdMatch::Extended;
	config.matchVal = 0;
	config.mask = 0;

	bool ok = mcp.filterSetConfig(0, config);
	EXPECT_TRUE(ok);
}

TEST(filter, setConfig_maskSid) {
	ostr.str("");
	pseudoSpi.reset();

	Filter::Config config;
	config.fifo = 1;
	config.typeId = Filter::TypeIdMatch::Standard;
	config.matchVal = 0;
	config.mask = 0xffffffff;

	bool ok = mcp.filterSetConfig(0, config);

	using spi = PseudoSpi;
	std::string expected = spi::strSelecet()
			+ spi::strWrite({0x21, 0xf0})	+ spi::strWrite({0x00, 0x00, 0x00, 0x00, 0xff, 0x07, 0x00, 0x40})
			+ spi::strUnselecet();
	expected += spi::strSelecet()
			+ spi::strWrite({0x31, 0xd0}) + spi::strRead({0x00})
			+ spi::strUnselecet();
	expected += spi::strSelecet()
							+ spi::strWrite({0x21, 0xd0}) + spi::strWrite({0x1})
			+ spi::strUnselecet();

	EXPECT_TRUE(ok);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(filter, setConfig_maskEid) {
	ostr.str("");
	pseudoSpi.reset();

	Filter::Config config;
	config.fifo = 1;
	config.typeId = Filter::TypeIdMatch::Extended;
	config.matchVal = 0;
	config.mask = 0xffffffff;

	bool ok = mcp.filterSetConfig(0, config);

	using spi = PseudoSpi;
	std::string expected = spi::strSelecet()
			+ spi::strWrite({0x21, 0xf0})	+ spi::strWrite({0x00, 0x00, 0x00, 0x40, 0xff, 0xff, 0xff, 0x5f})
			+ spi::strUnselecet();
	expected += spi::strSelecet()
			+ spi::strWrite({0x31, 0xd0}) + spi::strRead({0x00})
			+ spi::strUnselecet();
	expected += spi::strSelecet()
							+ spi::strWrite({0x21, 0xd0}) + spi::strWrite({0x1})
			+ spi::strUnselecet();

	EXPECT_TRUE(ok);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(filter, setConfig_matchValueSid) {
	ostr.str("");
	pseudoSpi.reset();

	Filter::Config config;
	config.fifo = 1;
	config.typeId = Filter::TypeIdMatch::Standard;
	config.matchVal = 0xffffffff;
	config.mask = 0;

	bool ok = mcp.filterSetConfig(0, config);

	using spi = PseudoSpi;
	std::string expected = spi::strSelecet()
			+ spi::strWrite({0x21, 0xf0})	+ spi::strWrite({0xff, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40})
			+ spi::strUnselecet();
	expected += spi::strSelecet()
			+ spi::strWrite({0x31, 0xd0}) + spi::strRead({0x00})
			+ spi::strUnselecet();
	expected += spi::strSelecet()
							+ spi::strWrite({0x21, 0xd0}) + spi::strWrite({0x1})
			+ spi::strUnselecet();

	EXPECT_TRUE(ok);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(filter, setConfig_matchValueEid) {
	ostr.str("");
	pseudoSpi.reset();

	Filter::Config config;
	config.fifo = 1;
	config.typeId = Filter::TypeIdMatch::Extended;
	config.matchVal = 0xffffffff;
	config.mask = 0x0;

	bool ok = mcp.filterSetConfig(0, config);

	using spi = PseudoSpi;
	std::string expected = spi::strSelecet()
			+ spi::strWrite({0x21, 0xf0})	+ spi::strWrite({0xff, 0xff, 0xff, 0x5f, 0x00, 0x00, 0x00, 0x40})
			+ spi::strUnselecet();
	expected += spi::strSelecet()
			+ spi::strWrite({0x31, 0xd0}) + spi::strRead({0x00})
			+ spi::strUnselecet();
	expected += spi::strSelecet()
							+ spi::strWrite({0x21, 0xd0}) + spi::strWrite({0x1})
			+ spi::strUnselecet();

	EXPECT_TRUE(ok);
	ASSERT_EQ(ostr.str(), expected);
}
