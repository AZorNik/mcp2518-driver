#include "gtest/gtest.h"
#include "mcp2518.hpp"
#include "pseudoSpi.hpp"

using namespace azn;
using namespace azn::mcp2518;

static std::ostringstream ostr;
static PseudoSpi spi(ostr);
static Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
		, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
		, [](bool cs){ return spi.select(cs); }
);

TEST(objToFrame, sid) {
	ReceiveMsgObj obj;
	ReceiveMsgObj::Ide.setField(obj.flags, false);
	ReceiveMsgObj::Sid.setField(obj.id, 0x3ff);

	uint8_t buf[8];
	can::Frame frame(buf, 8);
	bool ok = receiveObjToFrame(obj, frame);

	EXPECT_TRUE(ok);
	EXPECT_FALSE(frame.header.extId);
	EXPECT_EQ(obj.Sid.getField(obj.id), frame.header.id);
}

TEST(objToFrame, eid) {
	ReceiveMsgObj obj;
	ReceiveMsgObj::Ide.setField(obj.flags, true);
	ReceiveMsgObj::Id.setField(obj.id, 0x1fffffff);

	uint8_t buf[8];
	can::Frame frame(buf, 8);
	bool ok = receiveObjToFrame(obj, frame);

	EXPECT_TRUE(ok);
	EXPECT_TRUE(frame.header.extId);
	EXPECT_EQ(obj.Id.getField(obj.id), frame.header.id);
}

TEST(objToFrame, dataSize_0) {
	ReceiveMsgObj obj;
	ReceiveMsgObj::Dlc.setField(obj.flags, 0);

	uint8_t buf[8];
	can::Frame frame(buf, 8);
	bool ok = receiveObjToFrame(obj, frame);

	EXPECT_TRUE(ok);
	EXPECT_EQ(frame.header.dataSize, can::DataSize::Size_0);
}

TEST(objToFrame, dataSize_8) {
	ReceiveMsgObj obj;
	ReceiveMsgObj::Dlc.setField(obj.flags, 8);

	uint8_t buf[8];
	can::Frame frame(buf, 8);
	bool ok = receiveObjToFrame(obj, frame);

	EXPECT_TRUE(ok);
	EXPECT_EQ(frame.header.dataSize, can::DataSize::Size_8);
}

TEST(objToFrame, data_8Bytes) {
	ReceiveMsgObj obj;
	ReceiveMsgObj::Dlc.setField(obj.flags, 8);
	uint8_t data[8] = {1, 2, 3, 4, 5, 6, 7, 8};
	for (auto i = 0; i < 8; i++) {
		reinterpret_cast<uint8_t*>(&obj.val[3])[i] = data[i];
	}

	uint8_t buf[8];
	can::Frame frame(buf, 8);
	bool ok = receiveObjToFrame(obj, frame);

	EXPECT_TRUE(ok);
	EXPECT_EQ(frame.data.size(), 8);
	for (auto i = 0; i < 8; i++) {
		EXPECT_EQ(frame.data[i], data[i]);
	}
}

TEST(objToFrame, data_64Bytes) {
	ReceiveMsgObj obj;
	ReceiveMsgObj::Dlc.setField(obj.flags, 0xf);
	for (auto i = 0; i < 64; i++) {
		reinterpret_cast<uint8_t*>(&obj.val[3])[i] = i;
	}

	uint8_t buf[64];
	can::Frame frame(buf, 64);
	bool ok = receiveObjToFrame(obj, frame);

	EXPECT_TRUE(ok);
	EXPECT_EQ(frame.data.size(), 64);
	for (auto i = 0; i < 64; i++) {
		EXPECT_EQ(frame.data[i], i);
	}
}

TEST(objToFrame, data_64Bytes_into_small_buffer) {
	ReceiveMsgObj obj;
	ReceiveMsgObj::Dlc.setField(obj.flags, 0xf);
	for (auto i = 0; i < 64; i++) {
		reinterpret_cast<uint8_t*>(&obj.val[3])[i] = i;
	}

	uint8_t buf[8];
	can::Frame frame(buf, 8);
	bool ok = receiveObjToFrame(obj, frame);

	EXPECT_FALSE(ok);
	EXPECT_EQ(frame.data.size(), 8);
	for (auto i = 0; i < frame.data.size(); i++) {
		EXPECT_EQ(frame.data[i], i);
	}
}

TEST(objToFrame, noSwitchRate) {
	ReceiveMsgObj obj;
	ReceiveMsgObj::Brs.setField(obj.flags, false);
	ReceiveMsgObj::Fdf.setField(obj.flags, true);

	uint8_t buf[8];
	can::Frame frame(buf, 8);
	bool ok = receiveObjToFrame(obj, frame);

	EXPECT_TRUE(ok);
	EXPECT_EQ(frame.header.brs, false);
}

TEST(objToFrame, switchRate) {
	ReceiveMsgObj obj;
	ReceiveMsgObj::Brs.setField(obj.flags, true);
	ReceiveMsgObj::Fdf.setField(obj.flags, true);

	uint8_t buf[8];
	can::Frame frame(buf, 8);
	bool ok = receiveObjToFrame(obj, frame);

	EXPECT_TRUE(ok);
	EXPECT_EQ(frame.header.brs, true);
}

TEST(receiveFrame, dataSize_0) {
	spi.reset();
	uint32_t reg = 0;
	Fifo::Status::TfnRfnIf.setField(reg, true);
	spi.mem.write(0x60, 1, {1});
	spi.mem.write(0x64, 4, {0x04, 0x00, 0x00, 0x00});

	ReceiveMsgObj obj;
	ReceiveMsgObj::Ide.setField(obj.flags, false);
	ReceiveMsgObj::Sid.setField(obj.id, 0x123);
	ReceiveMsgObj::Dlc.setField(obj.flags, 0);

	DataArray ar;
	for (size_t i = 0; i < sizeof(obj); i++){
		ar.push_back(reinterpret_cast<uint8_t*>(&obj)[i]);
	}
	spi.mem.write(0x404, sizeof(obj), ar);

	uint8_t buf[8];
	can::Frame frame(buf, 8);
	auto ok = mcp.receiveFrame(1, frame);

	EXPECT_TRUE(ok);
	EXPECT_FALSE(frame.header.extId);
	EXPECT_EQ(frame.header.id, 0x123);
	EXPECT_EQ(frame.header.dataSize, can::DataSize::Size_0);
}

TEST(receiveFrame, dataSize_8) {
	spi.reset();
	uint32_t reg = 0;
	Fifo::Status::TfnRfnIf.setField(reg, true);
	spi.mem.write(0x60, 1, {1});
	spi.mem.write(0x64, 4, {0x04, 0x00, 0x00, 0x00});

	ReceiveMsgObj obj;
	ReceiveMsgObj::Ide.setField(obj.flags, false);
	ReceiveMsgObj::Sid.setField(obj.id, 0x123);
	ReceiveMsgObj::Dlc.setField(obj.flags, 8);

	uint8_t data[8] = {0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0xde, 0xf0};
	for (size_t i = 0; i < sizeof(data); i++) {
		reinterpret_cast<uint8_t*>(&obj.val[3])[i] = data[i];
	}

	DataArray ar;
	for (size_t i = 0; i < sizeof(obj); i++){
		ar.push_back(reinterpret_cast<uint8_t*>(&obj)[i]);
	}
	spi.mem.write(0x404, sizeof(obj), ar);

	uint8_t buf[8];
	can::Frame frame(buf, 8);
	auto ok = mcp.receiveFrame(1, frame);

	EXPECT_TRUE(ok);
	EXPECT_FALSE(frame.header.extId);
	EXPECT_EQ(frame.header.id, 0x123);
	EXPECT_EQ(frame.header.dataSize, can::DataSize::Size_8);
	EXPECT_EQ(frame.data.size(), 8);
	for (size_t i = 0; i < frame.data.size(); i++) {
		EXPECT_EQ(frame.data[i], data[i]);
	}
}

TEST(receiveFrame, dataSize_64) {
	spi.reset();
	uint32_t reg = 0;
	Fifo::Status::TfnRfnIf.setField(reg, true);
	spi.mem.write(0x60, 1, {1});
	spi.mem.write(0x64, 4, {0x04, 0x00, 0x00, 0x00});

	ReceiveMsgObj obj;
	ReceiveMsgObj::Ide.setField(obj.flags, false);
	ReceiveMsgObj::Sid.setField(obj.id, 0x123);
	ReceiveMsgObj::Dlc.setField(obj.flags, 0xF);

	for (size_t i = 0; i < 64; i++) {
		reinterpret_cast<uint8_t*>(&obj.val[3])[i] = i;
	}

	DataArray ar;
	for (size_t i = 0; i < sizeof(obj); i++){
		ar.push_back(reinterpret_cast<uint8_t*>(&obj)[i]);
	}
	spi.mem.write(0x404, sizeof(obj), ar);

	uint8_t buf[64];
	can::Frame frame(buf, 64);
	auto ok = mcp.receiveFrame(1, frame);

	EXPECT_TRUE(ok);
	EXPECT_FALSE(frame.header.extId);
	EXPECT_EQ(frame.header.id, 0x123);
	EXPECT_EQ(frame.header.dataSize, can::DataSize::Size_64);
	EXPECT_EQ(frame.data.size(), 64);
	for (size_t i = 0; i < frame.data.size(); i++) {
		EXPECT_EQ(frame.data[i], i);
	}
}
