#include "gtest/gtest.h"
#include "mcp2518.hpp"
#include "pseudoSpi.hpp"

using namespace azn;
using namespace azn::mcp2518;

static std::ostringstream ostr;
static PseudoSpi spi(ostr);
static Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
		, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
		, [](bool cs){ return spi.select(cs); }
);

TEST(objToCanFrame, sid) {
	ReceiveMsgObj obj;
	ReceiveMsgObj::Ide.setField(obj.flags, false);
	ReceiveMsgObj::Sid.setField(obj.id, 0x3ff);

	auto frame = receiveObjToCanFrame(obj);

	EXPECT_FALSE(frame.extId);
	EXPECT_EQ(obj.Sid.getField(obj.id), frame.id);
}

TEST(objToCanFrame, eid) {
	ReceiveMsgObj obj;
	ReceiveMsgObj::Ide.setField(obj.flags, true);
	ReceiveMsgObj::Id.setField(obj.id, 0x1fffffff);

	auto frame = receiveObjToCanFrame(obj);

	EXPECT_TRUE(frame.extId);
	EXPECT_EQ(obj.Id.getField(obj.id), frame.id);
}

TEST(objToCanFrame, noRtr) {
	ReceiveMsgObj obj;
	ReceiveMsgObj::Rtr.setField(obj.flags, false);

	auto frame = receiveObjToCanFrame(obj);

	EXPECT_FALSE(frame.rtr);
}

TEST(objToCanFrame, rtr) {
	ReceiveMsgObj obj;
	ReceiveMsgObj::Rtr.setField(obj.flags, true);

	auto frame = receiveObjToCanFrame(obj);

	EXPECT_TRUE(frame.rtr);
}

TEST(objToCanFrame, dataSize_0) {
	ReceiveMsgObj obj;
	ReceiveMsgObj::Dlc.setField(obj.flags, 0);

	auto frame = receiveObjToCanFrame(obj);

	EXPECT_EQ(frame.dataSize, can::DataSize::Size_0);
}

TEST(objToCanFrame, dataSize_8) {
	ReceiveMsgObj obj;
	ReceiveMsgObj::Dlc.setField(obj.flags, 8);

	auto frame = receiveObjToCanFrame(obj);

	EXPECT_EQ(frame.dataSize, can::DataSize::Size_8);
}

TEST(objToCanFrame, data_8Bytes) {
	ReceiveMsgObj obj;
	ReceiveMsgObj::Dlc.setField(obj.flags, 8);
	uint8_t data[8] = {1, 2, 3, 4, 5, 6, 7, 8};
	for (auto i = 0; i < 8; i++) {
		reinterpret_cast<uint8_t*>(&obj.val[3])[i] = data[i];
	}

	auto frame = receiveObjToCanFrame(obj);

	for (auto i = 0; i < 8; i++) {
		EXPECT_EQ(frame.data[i], data[i]);
	}
}

TEST(receiveCanFrame, dataSize_0) {
	spi.reset();

	uint32_t reg = 0;
	Fifo::Status::TfnRfnIf.setField(reg, true);
	spi.mem.write(0x60, 1, {1});
	spi.mem.write(0x64, 4, {0x04, 0x00, 0x00, 0x00});

	ReceiveMsgObj obj;
	ReceiveMsgObj::Ide.setField(obj.flags, false);
	ReceiveMsgObj::Sid.setField(obj.id, 0x123);
	ReceiveMsgObj::Rtr.setField(obj.flags, false);
	ReceiveMsgObj::Dlc.setField(obj.flags, 0);

	DataArray ar;
	for (size_t i = 0; i < sizeof(obj); i++){
		ar.push_back(reinterpret_cast<uint8_t*>(&obj)[i]);
	}
	spi.mem.write(0x404, sizeof(obj), ar);

	can::Can2Frame frame;
	auto ok = mcp.receiveFrame(1, frame);

	EXPECT_TRUE(ok);
	EXPECT_FALSE(frame.extId);
	EXPECT_EQ(frame.id, 0x123);
	EXPECT_EQ(frame.dataSize, can::DataSize::Size_0);
}

TEST(receiveCanFrame, dataSize_8) {
	spi.reset();

	uint32_t reg = 0;
	Fifo::Status::TfnRfnIf.setField(reg, true);
	spi.mem.write(0x60, 1, {1});
	spi.mem.write(0x64, 4, {0x04, 0x00, 0x00, 0x00});

	ReceiveMsgObj obj;
	ReceiveMsgObj::Ide.setField(obj.flags, false);
	ReceiveMsgObj::Sid.setField(obj.id, 0x123);
	ReceiveMsgObj::Rtr.setField(obj.flags, false);
	ReceiveMsgObj::Dlc.setField(obj.flags, 8);

	uint8_t data[8] = {0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0xde, 0xf0};
	for (size_t i = 0; i < sizeof(data); i++) {
		reinterpret_cast<uint8_t*>(&obj.val[3])[i] = data[i];
	}

	DataArray ar;
	for (size_t i = 0; i < sizeof(obj); i++){
		ar.push_back(reinterpret_cast<uint8_t*>(&obj)[i]);
	}
	spi.mem.write(0x404, sizeof(obj), ar);

	can::Can2Frame frame;
	auto ok = mcp.receiveFrame(1, frame);

	EXPECT_TRUE(ok);
	EXPECT_FALSE(frame.extId);
	EXPECT_EQ(frame.id, 0x123);
	EXPECT_EQ(frame.dataSize, can::DataSize::Size_8);
	for (size_t i = 0; i < dataSizeToBytesQty(frame.dataSize); i++) {
		EXPECT_EQ(frame.data[i], data[i]);
	}
}
