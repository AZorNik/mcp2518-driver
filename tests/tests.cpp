#include "gtest/gtest.h"
#include "mcp2518.hpp"
#include "pseudoSpi.hpp"

static std::ostringstream ostr;
static PseudoSpi spi(ostr);
//McpMemory mcpMem();

using namespace azn::mcp2518;

TEST(spi_readReg, 1Byte) {
	spi.reset();
	Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
			, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
			, [](bool cs){ return spi.select(cs); }
	);
	uint8_t buffer[8];

	auto ok = mcp.readReg(0xe14, buffer, 1);

	const std::string expected = "SPI select: true\nSPI write: size 2,\n\t0x3e 0x14\nSPI read: size 1,\n\t0x00\nSPI select: false\n";
	ASSERT_EQ(ok, true);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(spi_readReg, 3Bytes) {
	ostr.str("");
	spi.reset();
	Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
			, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
			, [](bool cs){ return spi.select(cs); }
	);
	uint8_t buffer[8];

	auto ok = mcp.readReg(0xe14, buffer, 3);

	const std::string expected = "SPI select: true\nSPI write: size 2,\n\t0x3e 0x14\nSPI read: size 3,\n\t0x00 0x00 0x00\nSPI select: false\n";
	ASSERT_EQ(ok, true);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(spi_writeReg, 1Byte) {
	ostr.str("");
	spi.reset();
	Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
			, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
			, [](bool cs){ return spi.select(cs); }
	);
	uint8_t buffer[8] = {0x12};

	auto ok = mcp.writeReg(0x3c, buffer, 1);

	const std::string expected = "SPI select: true\nSPI write: size 2,\n\t0x20 0x3c\nSPI write: size 1,\n\t0x12\nSPI select: false\n";
	ASSERT_EQ(ok, true);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(spi_writeReg, 3Bytes) {
	ostr.str("");
	spi.reset();
	Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
			, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
			, [](bool cs){ return spi.select(cs); }
	);
	uint8_t buffer[8] = {0x12, 0x1f, 0x58};

	auto ok = mcp.writeReg(0x3c, buffer, 3);

	const std::string expected = "SPI select: true\nSPI write: size 2,\n\t0x20 0x3c\nSPI write: size 3,\n\t0x12 0x1f 0x58\nSPI select: false\n";
	ASSERT_EQ(ok, true);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(spi_readMsgMem, 1Word) {
	ostr.str("");
	spi.reset();
	Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
			, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
			, [](bool cs){ return spi.select(cs); }
	);
	uint32_t buffer[8];

	auto ok = mcp.readMsgMem(0x468, buffer, 1);

	const std::string expected = "SPI select: true\nSPI write: size 2,\n\t0x34 0x68\nSPI read: size 4,\n\t0x00 0x00 0x00 0x00\nSPI select: false\n";
	ASSERT_EQ(ok, true);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(spi_readMsgMem, 3Words) {
	ostr.str("");
	spi.reset();
	Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
			, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
			, [](bool cs){ return spi.select(cs); }
	);
	uint32_t buffer[8];

	auto ok = mcp.readMsgMem(0x468, buffer, 3);

	const std::string expected = "SPI select: true\nSPI write: size 2,\n\t0x34 0x68\nSPI read: size 12,\n\t0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00\nSPI select: false\n";
	ASSERT_EQ(ok, true);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(spi_writeMsgMem, 1Word) {
	ostr.str("");
	spi.reset();
	Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
			, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
			, [](bool cs){ return spi.select(cs); }
	);
	uint32_t buffer[8] = {0x12};

	auto ok = mcp.writeMsgMem(0x468, buffer, 1);

	const std::string expected = "SPI select: true\nSPI write: size 2,\n\t0x24 0x68\nSPI write: size 4,\n\t0x12 0x00 0x00 0x00\nSPI select: false\n";
	ASSERT_EQ(ok, true);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(spi_writeMsgMem, 3Words) {
	ostr.str("");
	spi.reset();
	Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
			, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
			, [](bool cs){ return spi.select(cs); }
	);
	uint32_t buffer[8] = {0x12, 0x1f, 0x58};

	auto ok = mcp.writeMsgMem(0x468, buffer, 3);

	const std::string expected = "SPI select: true\nSPI write: size 2,\n\t0x24 0x68\nSPI write: size 12,\n\t0x12 0x00 0x00 0x00 0x1f 0x00 0x00 0x00 0x58 0x00 0x00 0x00\nSPI select: false\n";
	ASSERT_EQ(ok, true);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(Fifo, setConfig) {
	ostr.str("");
	spi.reset();
	Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
			, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
			, [](bool cs){ return spi.select(cs); }
	);

	Fifo::Config config;

	config.plSize = Fifo::PayloadSize::Data16Bytes;
	config.fifoSize = 5;
	config.retransmission = Fifo::Retransmission::ThreeRetransmission;
	config.priority = 16;
	config.direction = Fifo::Direction::Transmit;
	config.emptyFullInterruptEnable = true;
	config.notEmptyFullInterruptEnable = true;

	auto ok = mcp.setFifoConfig(1, config);

	const std::string expected = "SPI select: true\nSPI write: size 2,\n\t0x20 0x5c\nSPI write: size 4,\n\t0x85 0x00 0x30 0x44\nSPI select: false\n";
	ASSERT_EQ(ok, true);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(TransmitObject, write) {
	ostr.str("");
	spi.reset();
	Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
			, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
			, [](bool cs){ return spi.select(cs); }
	);
	TransmitMsgObj obj;

	TransmitMsgObj::Sid11.setField(obj.val[0], 1);
	TransmitMsgObj::Sid.setField(obj.val[0], 0x55);
	TransmitMsgObj::Fdf.setField(obj.val[1], 1);
	TransmitMsgObj::Ide.setField(obj.val[1], 0);
	TransmitMsgObj::Dlc.setField(obj.val[1], 10);

	uint32_t reg {0};
	Fifo::Status::TfnRfnIf.setField(reg, true);
	spi.mem.write(0x6c, 1 , {static_cast<uint8_t>(reg)});


	auto ok = mcp.writeMsgObj(2, obj);

	const std::string expected =
			PseudoSpi::strSelecet()
			+ PseudoSpi::strWrite({0x30, 0x6c})
			+ PseudoSpi::strRead({0x01})
			+ PseudoSpi::strUnselecet()
			+ PseudoSpi::strSelecet()
			+ PseudoSpi::strWrite({0x30, 0x70})
			+ PseudoSpi::strRead({0x00, 0x00})
			+ PseudoSpi::strUnselecet()
			+ PseudoSpi::strSelecet()
			+ PseudoSpi::strWrite({0x24, 0x00})
			+ PseudoSpi::strWrite({0x55, 0x00, 0x00, 0x20, 0x8a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00})
			+ PseudoSpi::strUnselecet()
			+ PseudoSpi::strSelecet()
			+ PseudoSpi::strWrite({0x20, 0x69})
			+ PseudoSpi::strWrite({0x01})
			+ PseudoSpi::strUnselecet();
	ASSERT_EQ(ok, true);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(ReceiveObject, read) {
	ostr.str("");
	spi.reset();
	Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
			, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
			, [](bool cs){ return spi.select(cs); }
	);
	ReceiveMsgObj obj;
	uint32_t reg {0};
	Fifo::Status::TfnRfnIf.setField(reg, true);
	spi.mem.write(0x78, 1 , {static_cast<uint8_t>(reg)});


	auto ok = mcp.readMsgObj(3, obj);

	const std::string expected =
			PseudoSpi::strSelecet()
			+ PseudoSpi::strWrite({0x30, 0x78})
			+ PseudoSpi::strRead({0x01})
			+ PseudoSpi::strUnselecet()
			+ PseudoSpi::strSelecet()
			+ PseudoSpi::strWrite({0x30, 0x7c})
			+ PseudoSpi::strRead({0x00, 0x00})
			+ PseudoSpi::strUnselecet()
			+ PseudoSpi::strSelecet()
			+ PseudoSpi::strWrite({0x34, 0x00})
			+ PseudoSpi::strRead({0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00})
			+ PseudoSpi::strUnselecet()
			+ PseudoSpi::strSelecet()
			+ PseudoSpi::strWrite({0x20, 0x75})
			+ PseudoSpi::strWrite({0x01})
			+ PseudoSpi::strUnselecet();
	ASSERT_EQ(ok, true);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(NominalBitTime, setOk) {
	ostr.str("");
	spi.reset();
	Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
			, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
			, [](bool cs){ return spi.select(cs); }
	);

	BitTime bitTime;

	bitTime.prescaler = 1;
	bitTime.tseg1 = 2;
	bitTime.tseg2 = 127;
	bitTime.sjw = 127;

	bool ok = mcp.setNominalBitTime(bitTime);

	const std::string expected = "SPI select: true\nSPI write: size 2,\n\t0x20 0x04\nSPI write: size 4,\n\t0x7f 0x7f 0x02 0x01\nSPI select: false\n";
	ASSERT_EQ(ok, true);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(NominalBitTime, setIncorrectTseg2) {
	ostr.str("");
	spi.reset();
	Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
			, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
			, [](bool cs){ return spi.select(cs); }
	);

	BitTime bitTime;

	bitTime.prescaler = 1;
	bitTime.tseg1 = 2;
	bitTime.tseg2 = 128;
	bitTime.sjw = 4;

	bool ok = mcp.setNominalBitTime(bitTime);

	const std::string expected = "";
	ASSERT_EQ(ok, false);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(NominalBitTime, setIncorrectSjw) {
	ostr.str("");
	spi.reset();
	Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
			, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
			, [](bool cs){ return spi.select(cs); }
	);

	BitTime bitTime;

	bitTime.prescaler = 1;
	bitTime.tseg1 = 2;
	bitTime.tseg2 = 3;
	bitTime.sjw = 128;

	bool ok = mcp.setNominalBitTime(bitTime);

	const std::string expected = "";
	ASSERT_EQ(ok, false);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(NominalBitTime, get) {
	ostr.str("");
	spi.reset();
	Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
			, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
			, [](bool cs){ return spi.select(cs); }
	);

	BitTime bitTime;

	bool ok = mcp.getNominalBitTime(bitTime);

	const std::string expected =
			PseudoSpi::strSelecet()
			+ PseudoSpi::strWrite({0x30, 0x04})
			+ PseudoSpi::strRead({0x0f, 0x0f, 0x3f, 0})
			+ PseudoSpi::strUnselecet();
			//"SPI select: true\nSPI write: size 2,\n\t0x30 0x04\nSPI read: size 4,\n\t0x00 0x00 0x00 0x00\nSPI select: false\n";
	ASSERT_EQ(ok, true);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(DataBitTime, setOk) {
	ostr.str("");
	spi.reset();
	Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
			, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
			, [](bool cs){ return spi.select(cs); }
	);

	BitTime bitTime;

	bitTime.prescaler = 1;
	bitTime.tseg1 = 31;
	bitTime.tseg2 = 15;
	bitTime.sjw = 15;

	bool ok = mcp.setDataBitTime(bitTime);

	const std::string expected = "SPI select: true\nSPI write: size 2,\n\t0x20 0x08\nSPI write: size 4,\n\t0x0f 0x0f 0x1f 0x01\nSPI select: false\n";
	ASSERT_EQ(ok, true);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(DataBitTime, setIncorrectTseg1) {
	ostr.str("");
	spi.reset();
	Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
			, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
			, [](bool cs){ return spi.select(cs); }
	);

	BitTime bitTime;

	bitTime.prescaler = 1;
	bitTime.tseg1 = 32;
	bitTime.tseg2 = 3;
	bitTime.sjw = 4;

	bool ok = mcp.setDataBitTime(bitTime);

	const std::string expected = "";
	ASSERT_EQ(ok, false);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(DataBitTime, setIncorrectTseg2) {
	ostr.str("");
	spi.reset();
	Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
			, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
			, [](bool cs){ return spi.select(cs); }
	);

	BitTime bitTime;

	bitTime.prescaler = 1;
	bitTime.tseg1 = 2;
	bitTime.tseg2 = 16;
	bitTime.sjw = 4;

	bool ok = mcp.setDataBitTime(bitTime);

	const std::string expected = "";
	ASSERT_EQ(ok, false);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(DataBitTime, setIncorrectSjw) {
	ostr.str("");
	spi.reset();
	Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
			, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
			, [](bool cs){ return spi.select(cs); }
	);

	BitTime bitTime;

	bitTime.prescaler = 1;
	bitTime.tseg1 = 2;
	bitTime.tseg2 = 3;
	bitTime.sjw = 16;

	bool ok = mcp.setDataBitTime(bitTime);

	const std::string expected = "";
	ASSERT_EQ(ok, false);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(DataBitTime, get) {
	ostr.str("");
	spi.reset();
	Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
			, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
			, [](bool cs){ return spi.select(cs); }
	);

	BitTime bitTime;

	bool ok = mcp.getDataBitTime(bitTime);

	const std::string expected =
			PseudoSpi::strSelecet()
			+ PseudoSpi::strWrite({0x30, 0x08})
			+ PseudoSpi::strRead({0x03, 0x03, 0x0e, 0x00})
			+ PseudoSpi::strUnselecet();
	ASSERT_EQ(ok, true);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(Request,MessageSending) {
	ostr.str("");
	spi.reset();
	Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
			, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
			, [](bool cs){ return spi.select(cs); }
	);

	bool ok = mcp.msgSendRequest(2);

	const std::string expected = "SPI select: true\nSPI write: size 2,\n\t0x20 0x69\nSPI write: size 1,\n\t0x02\nSPI select: false\n";
	ASSERT_EQ(ok, true);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(Osc, setConfig) {
	ostr.str("");
	spi.reset();
	Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
			, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
			, [](bool cs){ return spi.select(cs); }
	);

	Osc::Config config;
	config.outputDiv = Osc::OutputDivisor::Div1;
	config.systemClkDiv = Osc::SystemClkDivisor::Div2;
	config.lowPowerModeEn = true;
	config.pllEn = true;

	bool ok = mcp.setOscConfig(config);

	const std::string expected = spi.strSelecet()
			+ spi.strWrite({0x2e, 0x00})
			+ spi.strWrite({0x19, 0x00, 0x00, 0x00})
			+ spi.strUnselecet();
	ASSERT_EQ(ok, true);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(Osc, getConfig) {
	ostr.str("");
	spi.reset();
	Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
			, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
			, [](bool cs){ return spi.select(cs); }
	);

	Osc::Config config;

	bool ok = mcp.getOscConfig(config);

	const std::string expected = spi.strSelecet()
			+ spi.strWrite({0x3e, 0x00})
			+ spi.strRead({0x60, 0x00, 0x00, 0x00})
			+ spi.strUnselecet();
	ASSERT_EQ(ok, true);
	ASSERT_EQ(ostr.str(), expected);
	EXPECT_EQ(config.outputDiv, Osc::OutputDivisor::Div10);
	EXPECT_EQ(config.systemClkDiv, Osc::SystemClkDivisor::Div1);
	EXPECT_EQ(config.lowPowerModeEn, false);
	EXPECT_EQ(config.pllEn, false);
}

TEST(Osc, getFlags) {
	ostr.str("");
	spi.reset();
	Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
			, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
			, [](bool cs){ return spi.select(cs); }
	);

	spi.mem.write(0xe01, 1, {0xf3});

	Osc::Flags flags;
	bool ok = mcp.getOscFlags(flags);


	const std::string expected = spi.strSelecet()
			+ spi.strWrite({0x3e, 0x00})
			+ spi.strRead({0x60, 0xf3, 0x00, 0x00})
			+ spi.strUnselecet();

	ASSERT_EQ(ok, true);
	ASSERT_EQ(ostr.str(), expected);
	EXPECT_EQ(flags.oscReady, false);
	EXPECT_EQ(flags.pllReady, true);
	EXPECT_EQ(flags.sysClkReady, true);
}
