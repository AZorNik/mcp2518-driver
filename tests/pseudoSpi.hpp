#pragma once

#include <iostream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <cstring>

using DataArray = std::vector<uint8_t>;
struct McpMemory {

	struct Area {
		Area(uint16_t start, uint16_t size) : start(start), size(size), data (size) {}
		uint16_t start;
		uint16_t size;
		std::vector<uint8_t> data;
	};

	static constexpr uint16_t CanFdSfrStart = 0x00;
	static constexpr uint16_t CanFdSfrSize = 752;
	static constexpr uint16_t RamStart = 0x400;
	static constexpr uint16_t RamSize = 2048;
	static constexpr uint16_t McpStart = 0xe00;
	static constexpr uint16_t McpSize = 24;

	enum Areas {
		FdSfr,
		Ram,
		Mcp
	};

	McpMemory(void)
			: areas{{CanFdSfrStart, CanFdSfrSize}, {RamStart, RamSize}, {McpStart, McpSize}}
		{
		resetRegisters();
		cleanRam();
	}

	std::vector<Area> areas;

	bool spiWrite(const DataArray& data);
	bool spiRead(DataArray& data);

	void cleanRam(void);
	void resetRegisters(void);

	bool read(uint16_t addr, size_t size, DataArray& buffer);
	bool write(uint16_t addr, size_t size, const DataArray& buffer);

	static bool inArea(const Area& area, uint16_t addr) {
		return (addr >= area.start) && (addr < (area.start + area.size));
	}

	auto findArea(uint16_t addr, uint16_t size);

	enum Command {
		Reset,
		Write = 2,
		Read = 3,

		IncorrectValue,
	};

	bool parseCmd(const DataArray& data);
	Command lastCommand {Reset};
	uint16_t lastCommandAddr {0};

};

class PseudoSpi {
public:
	PseudoSpi(std::ostream& os) : os(os) {}
	bool select(bool cs) {
		if (!cs){
			mem.lastCommand = McpMemory::Reset;
		}
		os << (cs ? strSelecet() : strUnselecet());
		return cs;
	}

	bool read(uint8_t* buffer, uint8_t qtyRead) {
		auto data = buildVector(buffer, qtyRead);
		bool ok = mem.spiRead(data);
		os << strRead(data);

		for (uint8_t i = 0; i < qtyRead; i++) {
			buffer[i] = data[i];
		}

		return ok;
	}

	bool write(const uint8_t* buffer, uint8_t qtyWrite) {
		auto data = buildVector(buffer, qtyWrite);
		bool ok = mem.spiWrite(data);
		os << strWrite(data);
		return ok;
	}

	void reset(void) {
		os.clear();
		mem.resetRegisters();
		mem.cleanRam();
		mem.lastCommand = McpMemory::Reset;
	}

	static std::string strSelecet(void) { return "SPI select: true\n"; }
	static std::string strUnselecet(void) { return "SPI select: false\n"; }
	static std::string strWrite(const std::vector<uint8_t>& data) {
		std::string res = "SPI write: size " + std::to_string(data.size()) + ",\n";
		 res += printBytes(data);

		return res;
	}

	static std::string strRead(const std::vector<uint8_t>& data) {
		std::string res = "SPI read: size " + std::to_string(data.size()) + ",\n";
		res += printBytes(data);
		return res;
	}


	McpMemory mem;
private:
	std::ostream& os;


	static std::vector<uint8_t> buildVector(const uint8_t* data, uint8_t qty) {
		std::vector<uint8_t> vec;
		for (uint8_t i = 0; i < qty; i++) {
			vec.push_back(data[i]);
		}
		return vec;
	}

	static std::string printBytes(const std::vector<uint8_t> data) {
			std::stringstream res;
			res << "\t";
			bool first = true;
			for (const auto& byte : data) {
				if (first) {
					first = false;
				}
				else {
					res << " ";
				}

				res << "0x" << std::setfill('0') << std::setw(2) << std::hex << static_cast<unsigned>(byte);
			}
			res << "\n";
			return res.str();
		}
};


