#include "gtest/gtest.h"
#include "mcp2518.hpp"
#include "pseudoSpi.hpp"

using namespace azn;
using namespace azn::mcp2518;

static std::ostringstream ostr;
static PseudoSpi spi(ostr);
static Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
		, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
		, [](bool cs){ return spi.select(cs); }
);

TEST(canFrameToObj, sid) {
	can::Can2Frame frame;
	frame.extId = false;
	frame.id = 0x755;

	auto obj = canFrameToTransmitObj(frame);

	EXPECT_EQ(obj.Sid.getField(obj.val[0]), frame.id);
	EXPECT_EQ(obj.Sid11.getField(obj.val[0]), 0);
	EXPECT_EQ(obj.Eid.getField(obj.val[0]), 0);
	EXPECT_EQ(obj.Ide.getField(obj.val[1]), 0);
}

TEST(canFrameToObj, eid) {
	can::Can2Frame frame;
	frame.extId = true;
	frame.id = 0x755;

	auto obj = canFrameToTransmitObj(frame);

	EXPECT_EQ(obj.Sid11.getField(obj.val[0]), 0);
	EXPECT_EQ(obj.Id.getField(obj.val[0]), frame.id);
	EXPECT_EQ(obj.Ide.getField(obj.val[1]), 1);
}

TEST(canFrameToObj, noRtr) {
	can::Can2Frame frame;
	frame.rtr = false;

	auto obj = canFrameToTransmitObj(frame);

	EXPECT_EQ(obj.Rtr.getField(obj.val[1]), 0);
}

TEST(canFrameToObj, rtr) {
	can::Can2Frame frame;
	frame.rtr = true;

	auto obj = canFrameToTransmitObj(frame);

	EXPECT_EQ(obj.Rtr.getField(obj.val[1]), 1);
}

TEST(canFrameToObj, dataSize_0) {
	can::Can2Frame frame;
	frame.dataSize = can::DataSize::Size_0;

	auto obj = canFrameToTransmitObj(frame);

	EXPECT_EQ(obj.Dlc.getField(obj.val[1]), 0);
}

TEST(canFrameToObj, dataSize_8) {
	can::Can2Frame frame;
	frame.dataSize = can::DataSize::Size_8;

	auto obj = canFrameToTransmitObj(frame);

	EXPECT_EQ(obj.Dlc.getField(obj.val[1]), 8);
}

TEST(canFrameToObj, data_8Bytes) {
	can::Can2Frame frame;
	frame.dataSize = can::DataSize::Size_8;
	uint8_t data[8] = {1, 2, 3, 4, 5, 6, 7, 8};
	for (auto i = 0; i < 8; i++) {
		frame.data[i] = data[i];
	}

	auto obj = canFrameToTransmitObj(frame);


	for (auto i = 0; i < 8; i++) {
		EXPECT_EQ(reinterpret_cast<uint8_t*>(&obj.val)[8 + i], data[i]);
	}
}

TEST(sendCanFrame, dataSize_0) {
	spi.reset();
	can::Can2Frame frame;
	frame.dataSize = can::DataSize::Size_0;

	uint32_t reg {0};
	Fifo::Status::TfnRfnIf.setField(reg, true);
	spi.mem.write(0x60, 1 , {static_cast<uint8_t>(reg)});
	auto ok = mcp.sendFrame(1, frame);

	EXPECT_TRUE(ok);
}

TEST(sendCanFrame, dataSize_8) {
	spi.reset();
	can::Can2Frame frame;
	frame.dataSize = can::DataSize::Size_8;

	uint32_t reg {0};
	Fifo::Status::TfnRfnIf.setField(reg, true);
	spi.mem.write(0x60, 1 , {static_cast<uint8_t>(reg)});
	auto ok = mcp.sendFrame(1, frame);

	EXPECT_TRUE(ok);
}

TEST(sendCanFrame, tooMuchData) {
	can::Can2Frame frame;
	frame.dataSize = can::DataSize::Size_12;

	auto ok = mcp.sendFrame(1, frame);

	EXPECT_FALSE(ok);
}

TEST(sendCanFrame, incorrectFifo) {
	can::Can2Frame frame;
	frame.dataSize = can::DataSize::Size_0;

	auto ok = mcp.sendFrame(0, frame);

	EXPECT_FALSE(ok);
}
