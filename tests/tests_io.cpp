#include "gtest/gtest.h"
#include "mcp2518.hpp"
#include "pseudoSpi.hpp"

using namespace azn::mcp2518;

static std::ostringstream ostr;
static PseudoSpi pseudoSpi(ostr);
static Driver mcp([](uint8_t* buffer, uint8_t qty) { return pseudoSpi.read(buffer, qty); }
		, [](const uint8_t* buffer, uint8_t qty) { return pseudoSpi.write(buffer, qty); }
		, [](bool cs){ return pseudoSpi.select(cs); }
);

using Spi = PseudoSpi;

TEST(io, set_config) {
	ostr.str("");
	pseudoSpi.reset();

	Io::Config config;

	config.autoTransceiverStandbyEn = true;
	config.clkoPinFunction = Io::ClkoPinFunction::StartOfFrame;
	config.interruptPinsMode = Io::PinMode::OpenDrain;
	config.txPinMode = Io::PinMode::OpenDrain;
	config.pin1Function = Io::PinFunction::Interrrupt;
	config.pin0Function = Io::PinFunction::Interrrupt;
	config.pin1Direction = Io::PinDirection::Output;
	config.pin0Direction = Io::PinDirection::Output;


	bool ok = mcp.ioSetConfig(config);

	std::string expected = Spi::strSelecet()
			+ Spi::strWrite({0x3e, 0x04}) + Spi::strRead({0x03, 0x00, 0x00, 0x03})
			+ Spi::strUnselecet();
	expected += Spi::strSelecet()
			+ Spi::strWrite({0x2e, 0x04})	+ Spi::strWrite({0x40, 0x00, 0x00, 0x70})
			+ Spi::strUnselecet();

	EXPECT_TRUE(ok);
	ASSERT_EQ(ostr.str(), expected);
}

TEST(io, get_config) {
	ostr.str("");
	pseudoSpi.reset();

	Io::Config config;

	pseudoSpi.mem.write(0xe04, 4, {0x43, 0x00, 0x00, 0x73});

	bool ok = mcp.ioGetConfig(config);

	std::string expected = Spi::strSelecet()
			+ Spi::strWrite({0x3e, 0x04}) + Spi::strRead({0x43, 0x00, 0x00, 0x73})
			+ Spi::strUnselecet();

	EXPECT_TRUE(ok);
	ASSERT_EQ(ostr.str(), expected);
	EXPECT_EQ(config.autoTransceiverStandbyEn, true);
	EXPECT_EQ(config.clkoPinFunction, Io::ClkoPinFunction::StartOfFrame);
	EXPECT_EQ(config.interruptPinsMode, Io::PinMode::OpenDrain);
	EXPECT_EQ(config.txPinMode, Io::PinMode::OpenDrain);
	EXPECT_EQ(config.pin1Function, Io::PinFunction::Gpio);
	EXPECT_EQ(config.pin0Function, Io::PinFunction::Gpio);
	EXPECT_EQ(config.pin1Direction, Io::PinDirection::Input);
	EXPECT_EQ(config.pin0Direction, Io::PinDirection::Input);
}

TEST(io, set_pin0) {
	ostr.str("");
	pseudoSpi.reset();

	Io::Config config;

	pseudoSpi.mem.write(0xe04, 4, {0x43, 0x00, 0x00, 0x73});

	bool ok = mcp.ioSetPinState(Io::PinId::Pin0, Io::PinState::Set);

	std::string expected = Spi::strSelecet()
			+ Spi::strWrite({0x3e, 0x04}) + Spi::strRead({0x43, 0x00, 0x00, 0x73})
			+ Spi::strUnselecet();
	expected = Spi::strSelecet()
			+ Spi::strWrite({0x3e, 0x04}) + Spi::strWrite({0x43, 0x00, 0x01, 0x73})
			+ Spi::strUnselecet();

	EXPECT_TRUE(ok);
}

TEST(io, reset_pin0) {
	ostr.str("");
	pseudoSpi.reset();

	Io::Config config;

	pseudoSpi.mem.write(0xe04, 4, {0x43, 0x00, 0x01, 0x73});

	bool ok = mcp.ioSetPinState(Io::PinId::Pin0, Io::PinState::Set);

	std::string expected = Spi::strSelecet()
			+ Spi::strWrite({0x3e, 0x04}) + Spi::strRead({0x43, 0x00, 0x01, 0x73})
			+ Spi::strUnselecet();
	expected = Spi::strSelecet()
			+ Spi::strWrite({0x3e, 0x04}) + Spi::strWrite({0x43, 0x00, 0x00, 0x73})
			+ Spi::strUnselecet();

	EXPECT_TRUE(ok);
}

TEST(io, set_pin1) {
	ostr.str("");
	pseudoSpi.reset();

	Io::Config config;

	pseudoSpi.mem.write(0xe04, 4, {0x43, 0x00, 0x00, 0x73});

	bool ok = mcp.ioSetPinState(Io::PinId::Pin0, Io::PinState::Set);

	std::string expected = Spi::strSelecet()
			+ Spi::strWrite({0x3e, 0x04}) + Spi::strRead({0x43, 0x00, 0x00, 0x73})
			+ Spi::strUnselecet();
	expected = Spi::strSelecet()
			+ Spi::strWrite({0x3e, 0x04}) + Spi::strWrite({0x43, 0x00, 0x02, 0x73})
			+ Spi::strUnselecet();

	EXPECT_TRUE(ok);
}

TEST(io, reset_pin1) {
	ostr.str("");
	pseudoSpi.reset();

	Io::Config config;

	pseudoSpi.mem.write(0xe04, 4, {0x43, 0x00, 0x02, 0x73});

	bool ok = mcp.ioSetPinState(Io::PinId::Pin0, Io::PinState::Set);

	std::string expected = Spi::strSelecet()
			+ Spi::strWrite({0x3e, 0x04}) + Spi::strRead({0x43, 0x00, 0x02, 0x73})
			+ Spi::strUnselecet();
	expected = Spi::strSelecet()
			+ Spi::strWrite({0x3e, 0x04}) + Spi::strWrite({0x43, 0x00, 0x00, 0x73})
			+ Spi::strUnselecet();

	EXPECT_TRUE(ok);
}

TEST(io, get_pin0_state) {
	ostr.str("");
	pseudoSpi.reset();

	Io::Config config;

	pseudoSpi.mem.write(0xe06, 1, {0x01});

	Io::PinState state = Io::PinState::Reset;
	bool ok = mcp.ioGetPinState(Io::PinId::Pin0, state);

	EXPECT_TRUE(ok);
	EXPECT_EQ(state, Io::PinState::Set);
}

TEST(io, get_pin1_state) {
	ostr.str("");
	pseudoSpi.reset();

	Io::Config config;

	pseudoSpi.mem.write(0xe06, 1, {0x02});

	Io::PinState state = Io::PinState::Reset;
	bool ok = mcp.ioGetPinState(Io::PinId::Pin1, state);

	EXPECT_TRUE(ok);
	EXPECT_EQ(state, Io::PinState::Set);
}
