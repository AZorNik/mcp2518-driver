#include "gtest/gtest.h"
#include "mcp2518.hpp"
#include "pseudoSpi.hpp"

using namespace azn;
using namespace azn::mcp2518;

static std::ostringstream ostr;
static PseudoSpi spi(ostr);
static Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
		, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
		, [](bool cs){ return spi.select(cs); }
);

TEST(fdFrameToObj, sid) {
	can::FdFrame frame;
	frame.extId = false;
	frame.id = 0x755;

	auto obj = canFdFrameToTransmitObj(frame);

	EXPECT_EQ(obj.Sid.getField(obj.val[0]), frame.id);
	EXPECT_EQ(obj.Sid11.getField(obj.val[0]), 0);
	EXPECT_EQ(obj.Eid.getField(obj.val[0]), 0);
	EXPECT_EQ(obj.Ide.getField(obj.val[1]), 0);
	EXPECT_EQ(obj.Fdf.getField(obj.val[1]), 1);
}

TEST(fdFrameToObj, eid) {
	can::FdFrame frame;
	frame.extId = true;
	frame.id = 0x755;

	auto obj = canFdFrameToTransmitObj(frame);

	EXPECT_EQ(obj.Sid11.getField(obj.val[0]), 0);
	EXPECT_EQ(obj.Id.getField(obj.val[0]), frame.id);
	EXPECT_EQ(obj.Ide.getField(obj.val[1]), 1);
	EXPECT_EQ(obj.Fdf.getField(obj.val[1]), 1);
}

TEST(fdFrameToObj, dataSize_0) {
	can::FdFrame frame;
	frame.dataSize = can::DataSize::Size_0;

	auto obj = canFdFrameToTransmitObj(frame);

	EXPECT_EQ(obj.Dlc.getField(obj.val[1]), 0);
}

TEST(fdFrameToObj, dataSize_8) {
	can::FdFrame frame;
	frame.dataSize = can::DataSize::Size_8;

	auto obj = canFdFrameToTransmitObj(frame);

	EXPECT_EQ(obj.Dlc.getField(obj.val[1]), 8);
}

TEST(fdFrameToObj, data_8Bytes) {
	can::FdFrame frame;
	frame.dataSize = can::DataSize::Size_8;
	uint8_t data[8] = {1, 2, 3, 4, 5, 6, 7, 8};
	for (auto i = 0; i < 8; i++) {
		frame.data[i] = data[i];
	}

	auto obj = canFdFrameToTransmitObj(frame);


	for (auto i = 0; i < 8; i++) {
		EXPECT_EQ(reinterpret_cast<uint8_t*>(&obj.val)[8 + i], data[i]);
	}
}

TEST(fdFrameToObj, dataSize_64) {
	can::FdFrame frame;
	frame.dataSize = can::DataSize::Size_64;

	auto obj = canFdFrameToTransmitObj(frame);

	EXPECT_EQ(obj.Dlc.getField(obj.val[1]), 0xf);
}

TEST(fdFrameToObj, data_64Bytes) {
	can::FdFrame frame;
	frame.dataSize = can::DataSize::Size_64;

	for (auto i = 0; i < 64; i++) {
		frame.data[i] = i;
	}

	auto obj = canFdFrameToTransmitObj(frame);


	for (auto i = 0; i < 8; i++) {
		EXPECT_EQ(reinterpret_cast<uint8_t*>(&obj.val)[8 + i], i);
	}
}

TEST(sendCanFdFrame, dataSize_0) {
	spi.reset();
	can::FdFrame frame;
	frame.dataSize = can::DataSize::Size_0;

	uint32_t reg {0};
	Fifo::Status::TfnRfnIf.setField(reg, true);
	spi.mem.write(0x60, 1 , {static_cast<uint8_t>(reg)});
	auto ok = mcp.sendFrame(1, frame);

	EXPECT_TRUE(ok);
}

TEST(sendCanFdFrame, dataSize_8) {
	spi.reset();
	can::FdFrame frame;
	frame.dataSize = can::DataSize::Size_8;

	uint32_t reg {0};
	Fifo::Status::TfnRfnIf.setField(reg, true);
	spi.mem.write(0x60, 1 , {static_cast<uint8_t>(reg)});
	auto ok = mcp.sendFrame(1, frame);

	EXPECT_TRUE(ok);
}

TEST(sendCanFdFrame, dataSize_64) {
	can::FdFrame frame;
	frame.dataSize = can::DataSize::Size_64;

	auto ok = mcp.sendFrame(1, frame);

	EXPECT_TRUE(ok);
}

TEST(sendCanFdFrame, incorrectFifo) {
	can::FdFrame frame;
	frame.dataSize = can::DataSize::Size_0;

	auto ok = mcp.sendFrame(0, frame);

	EXPECT_FALSE(ok);
}
