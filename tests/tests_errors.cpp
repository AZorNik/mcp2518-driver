#include "gtest/gtest.h"
#include "mcp2518.hpp"
#include "pseudoSpi.hpp"

using namespace azn::mcp2518;

static std::ostringstream ostr;
static PseudoSpi pseudoSpi(ostr);
static Driver mcp([](uint8_t* buffer, uint8_t qty) { return pseudoSpi.read(buffer, qty); }
		, [](const uint8_t* buffer, uint8_t qty) { return pseudoSpi.write(buffer, qty); }
		, [](bool cs){ return pseudoSpi.select(cs); }
);

using Spi = PseudoSpi;

TEST(errors, get_trec) {
	ostr.str("");
	pseudoSpi.reset();

	pseudoSpi.mem.write(0x34, 4, {254, 200, 0x3f, 0});

	Errors::Trec trec;

	bool ok = mcp.errorsGetCounters(trec);

	std::string expected = Spi::strSelecet()
			+ Spi::strWrite({0x30, 0x34}) + Spi::strRead({254, 200, 0x3f, 0x00})
			+ Spi::strUnselecet();

	EXPECT_TRUE(ok);
	ASSERT_EQ(ostr.str(), expected);
	EXPECT_TRUE(trec.busOff);
	EXPECT_TRUE(trec.txPassive);
	EXPECT_TRUE(trec.rxPassive);
	EXPECT_TRUE(trec.txWarning);
	EXPECT_TRUE(trec.rxWarning);
	EXPECT_TRUE(trec.warning);
	EXPECT_EQ(trec.txErrorCounter, 200);
	EXPECT_EQ(trec.rxErrorCounter, 254);
}
