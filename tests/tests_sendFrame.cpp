#include "gtest/gtest.h"
#include "mcp2518.hpp"
#include "pseudoSpi.hpp"

using namespace azn;
using namespace azn::mcp2518;

static std::ostringstream ostr;
static PseudoSpi spi(ostr);
static Driver mcp([](uint8_t* buffer, uint8_t qty) { return spi.read(buffer, qty); }
		, [](const uint8_t* buffer, uint8_t qty) { return spi.write(buffer, qty); }
		, [](bool cs){ return spi.select(cs); }
);

TEST(frameToObj, not_fd_frame) {
	uint8_t buf[8];
	can::Frame frame(buf, 8);
	frame.header.fd = false;

	auto obj = frameToTransmitObj(frame);

	EXPECT_EQ(obj.Fdf.getField(obj.val[1]), false);
}

TEST(frameToObj, fd_frame) {
	uint8_t buf[8];
	can::Frame frame(buf, 8);
	frame.header.fd = true;

	auto obj = frameToTransmitObj(frame);

	EXPECT_EQ(obj.Fdf.getField(obj.val[1]), true);
}

TEST(frameToObj, sid) {
	uint8_t buf[8];
	can::Frame frame(buf, 8);
	frame.header.extId = false;
	frame.header.id = 0x755;

	auto obj = frameToTransmitObj(frame);

	EXPECT_EQ(obj.Sid.getField(obj.val[0]), frame.header.id);
	EXPECT_EQ(obj.Sid11.getField(obj.val[0]), 0);
	EXPECT_EQ(obj.Eid.getField(obj.val[0]), 0);
	EXPECT_EQ(obj.Ide.getField(obj.val[1]), false);
}

TEST(frameToObj, eid) {
	uint8_t buf[8];
	can::Frame frame(buf, 8);
	frame.header.extId = true;
	frame.header.id = 0x755;

	auto obj = frameToTransmitObj(frame);

	EXPECT_EQ(obj.Sid11.getField(obj.val[0]), 0);
	EXPECT_EQ(obj.Id.getField(obj.val[0]), frame.header.id);
	EXPECT_EQ(obj.Ide.getField(obj.val[1]), true);
}

TEST(frameToObj, no_rtr) {
	uint8_t buf[8];
	can::Frame frame(buf, 8);
	frame.header.rtr = false;
	frame.header.fd = false;

	auto obj = frameToTransmitObj(frame);

	EXPECT_EQ(obj.Rtr.getField(obj.val[1]), false);
}

TEST(frameToObj, rtr) {
	uint8_t buf[8];
	can::Frame frame(buf, 8);

	frame.header.rtr = true;
	frame.header.fd = false;

	auto obj = frameToTransmitObj(frame);

	EXPECT_EQ(obj.Rtr.getField(obj.val[1]), true);
}

TEST(frameToObj, no_brs) {
	uint8_t buf[8];
	can::Frame frame(buf, 8);
	frame.header.brs = false;
	frame.header.fd = true;

	auto obj = frameToTransmitObj(frame);

	EXPECT_EQ(obj.Brs.getField(obj.val[1]), false);
}

TEST(frameToObj, brs) {
	uint8_t buf[8];
	can::Frame frame(buf, 8);
	frame.header.brs = true;
	frame.header.fd = true;

	auto obj = frameToTransmitObj(frame);

	EXPECT_EQ(obj.Brs.getField(obj.val[1]), true);
}

TEST(frameToObj, dataSize_0) {
	uint8_t buf[8];
	can::Frame frame(buf, 8);
	frame.header.dataSize = can::DataSize::Size_0;

	auto obj = frameToTransmitObj(frame);

	EXPECT_EQ(obj.Dlc.getField(obj.val[1]), 0);
}

TEST(frameToObj, dataSize_8) {
	uint8_t buf[8];
	can::Frame frame(buf, 8);
	frame.header.dataSize = can::DataSize::Size_8;

	auto obj = frameToTransmitObj(frame);

	EXPECT_EQ(obj.Dlc.getField(obj.val[1]), 8);
}

TEST(frameToObj, data_8Bytes) {
	uint8_t buf[8];
	can::Frame frame(buf, 8);
	frame.header.dataSize = can::DataSize::Size_8;
	uint8_t data[8] = {1, 2, 3, 4, 5, 6, 7, 8};
	bool ok = true;
	for (auto i = 0; i < 8; i++) {
		ok = ok && frame.data.add(data[i]);
	}

	auto obj = frameToTransmitObj(frame);

	for (auto i = 0; i < 8; i++) {
		EXPECT_EQ(reinterpret_cast<uint8_t*>(&obj.val)[8 + i], data[i]);
	}
}

TEST(sendFrame, dataSize_0) {
	spi.reset();
	uint32_t reg {0};
	Fifo::Status::TfnRfnIf.setField(reg, true);
	spi.mem.write(0x60, 1 , {static_cast<uint8_t>(reg)});

	uint8_t buf[8];
	can::Frame frame(buf, 8);
	frame.header.dataSize = can::DataSize::Size_0;

	auto ok = mcp.sendFrame(1, frame);

	EXPECT_TRUE(ok);
}

TEST(sendFrame, dataSize_8) {
	spi.reset();
	uint32_t reg {0};
	Fifo::Status::TfnRfnIf.setField(reg, true);
	spi.mem.write(0x60, 1 , {static_cast<uint8_t>(reg)});

	uint8_t buf[8];
	can::Frame frame(buf, 8);
	frame.header.dataSize = can::DataSize::Size_8;

	auto ok = mcp.sendFrame(1, frame);

	EXPECT_TRUE(ok);
}

TEST(sendFrame, can2_tooMuchData) {
	uint8_t buf[8];
	can::Frame frame(buf, 8);
	frame.header.fd = false;
	frame.header.dataSize = can::DataSize::Size_12;

	auto ok = mcp.sendFrame(1, frame);

	EXPECT_FALSE(ok);
}

TEST(sendFrame, fd_dataSize_64) {
	spi.reset();
	uint32_t reg {0};
	Fifo::Status::TfnRfnIf.setField(reg, true);
	spi.mem.write(0x60, 1 , {static_cast<uint8_t>(reg)});

	uint8_t buf[64];
	can::Frame frame(buf, 64);
	frame.header.dataSize = can::DataSize::Size_64;
	frame.header.fd = true;

	auto ok = mcp.sendFrame(1, frame);

	EXPECT_TRUE(ok);
}

TEST(sendFrame, incorrectFifo) {
	spi.reset();
	uint32_t reg {0};
	Fifo::Status::TfnRfnIf.setField(reg, true);
	spi.mem.write(0x60, 1 , {static_cast<uint8_t>(reg)});

	uint8_t buf[8];
	can::Frame frame(buf, 8);

	auto ok = mcp.sendFrame(0, frame);

	EXPECT_FALSE(ok);
}
