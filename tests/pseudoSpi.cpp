#include "pseudoSpi.hpp"

bool McpMemory::parseCmd(const DataArray& data) {
	bool ok = data.size() == 2;

	if (ok) {
		lastCommand = static_cast<Command>(data[0] >> 4);
		if (lastCommand >= IncorrectValue) {
			lastCommand = Reset;
			ok = false;
		}
		else {
			lastCommandAddr = data[1] + (data[0] & 0xf) * 256;
		}
	}

	return ok;
}

bool McpMemory::spiWrite(const DataArray& data) {
	bool ok;
	switch (lastCommand) {
		case Reset :
			ok = parseCmd(data);
			break;
		case Write :
			ok = write(lastCommandAddr, data.size(), data);
			break;
		case Read:
			ok = parseCmd(data);
			break;
		case IncorrectValue:
			ok = false;
			break;
	}

	return ok;
}

bool McpMemory::spiRead(DataArray& data) {
	bool ok = lastCommand == Read;
	ok = ok && read(lastCommandAddr, data.size(), data);
	return ok;
}

auto McpMemory::findArea(uint16_t addr, uint16_t size) {
	auto it = areas.begin();
	for (; it != areas.end(); it++ ) {
		bool found = inArea(*it, addr) && inArea(*it, addr + size - 1);
		if (found) {
			break;
		}
	}

	return it;
}

bool McpMemory::read(uint16_t addr, size_t size, DataArray& data) {
	auto area = findArea(addr, size);
	bool ok = area != areas.end();

	if (data.size() < size) {
		data.resize(size);
	}
	if (ok) {
		auto from = addr - area->start;
		copy(area->data.begin() + from, area->data.begin() + from + size, data.begin());
		//memcpy(reinterpret_cast<void*>(buffer), reinterpret_cast<const void*>(&it->data[from]), size);
	}

	return ok;
}

bool McpMemory::write(uint16_t addr, size_t size, const DataArray& data) {
	auto area = findArea(addr, size);
	bool ok = area != areas.end();

	if (ok) {
		auto from = addr - area->start;
		copy(data.begin(), data.end(), area->data.begin() + from);
		//memcpy(reinterpret_cast<void*>(&it->data[from]), reinterpret_cast<const void*>(data.buffer), size);
	}

	return ok;
}

void McpMemory::cleanRam(void) {
	for (auto& byte : areas[Areas::Ram].data) {
		byte = 0;
	}
}

void McpMemory::resetRegisters(void) {
	for (auto& byte : areas[Areas::Mcp].data) {
		byte = 0;
	}

	areas[Areas::Mcp].data[0] = 0x60;
	areas[Areas::Mcp].data[4] = 0x03;
	areas[Areas::Mcp].data[7] = 0x03;

	for (auto& byte : areas[Areas::FdSfr].data) {
		byte = 0;
	}

	areas[Areas::FdSfr].data[0] = 0x60;
	areas[Areas::FdSfr].data[1] = 0x07;
	areas[Areas::FdSfr].data[2] = 0x98;
	areas[Areas::FdSfr].data[3] = 0x04;

	areas[Areas::FdSfr].data[4] = 0x0f;
	areas[Areas::FdSfr].data[5] = 0x0f;
	areas[Areas::FdSfr].data[6] = 0x3f;

	areas[Areas::FdSfr].data[8] = 0x03;
	areas[Areas::FdSfr].data[9] = 0x03;
	areas[Areas::FdSfr].data[10] = 0x0e;

	areas[Areas::FdSfr].data[13] = 0x10;
	areas[Areas::FdSfr].data[14] = 0x02;


	areas[1].data[24] = 0x40;
	areas[1].data[26] = 0x40;
	areas[1].data[27] = 0x40;

	areas[1].data[54] = 0x20;
	areas[1].data[65] = 0x04;
	areas[1].data[76] = 0x80;
	areas[1].data[77] = 0x04;
	areas[1].data[78] = 0x60;

	areas[1].data[80] = 0x05;
}
